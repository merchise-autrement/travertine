# expr_parsertab.py
# This file is automatically generated. Do not edit.
# pylint: disable=W,C,R
# flake8: noqa
_tabversion = "3.10"

_lr_method = "LALR"

_lr_signature = "exprADD FLOORDIV LITERAL_NUMBER LPAREN MUL RPAREN SUB SUBSTEP TRUEDIV UNQUOTED_VARIABLE VARIABLEexpr : expr ADD termexpr : expr SUB termexpr : termterm : term MUL factorterm : term FLOORDIV factorterm : term TRUEDIV factorterm : factorfactor : VARIABLEfactor : UNQUOTED_VARIABLEfactor : LITERAL_NUMBERfactor : SUB exprfactor : SUBSTEPfactor : LPAREN expr RPAREN"

_lr_action_items = {
    "VARIABLE": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            5,
            5,
            5,
            5,
            5,
            5,
            5,
            5,
        ],
    ),
    "UNQUOTED_VARIABLE": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            6,
            6,
            6,
            6,
            6,
            6,
            6,
            6,
        ],
    ),
    "LITERAL_NUMBER": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            7,
            7,
            7,
            7,
            7,
            7,
            7,
            7,
        ],
    ),
    "SUB": (
        [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11,
            12,
            13,
            14,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            3,
            11,
            -3,
            3,
            -7,
            -8,
            -9,
            -10,
            -12,
            3,
            3,
            3,
            3,
            3,
            3,
            11,
            11,
            -1,
            -2,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "SUBSTEP": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            8,
            8,
            8,
            8,
            8,
            8,
            8,
            8,
        ],
    ),
    "LPAREN": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            9,
            9,
            9,
            9,
            9,
            9,
            9,
            9,
        ],
    ),
    "$end": (
        [
            1,
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            0,
            -3,
            -7,
            -8,
            -9,
            -10,
            -12,
            -11,
            -1,
            -2,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "ADD": (
        [
            1,
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            10,
            -3,
            -7,
            -8,
            -9,
            -10,
            -12,
            10,
            10,
            -1,
            -2,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "MUL": (
        [
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            12,
            -7,
            -8,
            -9,
            -10,
            -12,
            -11,
            12,
            12,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "FLOORDIV": (
        [
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            13,
            -7,
            -8,
            -9,
            -10,
            -12,
            -11,
            13,
            13,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "TRUEDIV": (
        [
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            14,
            -7,
            -8,
            -9,
            -10,
            -12,
            -11,
            14,
            14,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
    "RPAREN": (
        [
            2,
            4,
            5,
            6,
            7,
            8,
            15,
            16,
            17,
            18,
            19,
            20,
            21,
            22,
        ],
        [
            -3,
            -7,
            -8,
            -9,
            -10,
            -12,
            -11,
            22,
            -1,
            -2,
            -4,
            -5,
            -6,
            -13,
        ],
    ),
}

_lr_action = {}
for _k, _v in _lr_action_items.items():
    for _x, _y in zip(_v[0], _v[1]):
        if not _x in _lr_action:
            _lr_action[_x] = {}
        _lr_action[_x][_k] = _y
del _lr_action_items

_lr_goto_items = {
    "expr": (
        [
            0,
            3,
            9,
        ],
        [
            1,
            15,
            16,
        ],
    ),
    "term": (
        [
            0,
            3,
            9,
            10,
            11,
        ],
        [
            2,
            2,
            2,
            17,
            18,
        ],
    ),
    "factor": (
        [
            0,
            3,
            9,
            10,
            11,
            12,
            13,
            14,
        ],
        [
            4,
            4,
            4,
            4,
            4,
            19,
            20,
            21,
        ],
    ),
}

_lr_goto = {}
for _k, _v in _lr_goto_items.items():
    for _x, _y in zip(_v[0], _v[1]):
        if not _x in _lr_goto:
            _lr_goto[_x] = {}
        _lr_goto[_x][_k] = _y
del _lr_goto_items
_lr_productions = [
    ("S' -> expr", "S'", 1, None, None, None),
    ("expr -> expr ADD term", "expr", 3, "p_add_expression", "parser.py", 99),
    ("expr -> expr SUB term", "expr", 3, "p_sub_expression", "parser.py", 105),
    ("expr -> term", "expr", 1, "p_term_expression", "parser.py", 111),
    ("term -> term MUL factor", "term", 3, "p_mul_term", "parser.py", 116),
    ("term -> term FLOORDIV factor", "term", 3, "p_floordiv_term", "parser.py", 122),
    ("term -> term TRUEDIV factor", "term", 3, "p_truediv_term", "parser.py", 128),
    ("term -> factor", "term", 1, "p_factor_term", "parser.py", 134),
    ("factor -> VARIABLE", "factor", 1, "p_factor_variable", "parser.py", 139),
    (
        "factor -> UNQUOTED_VARIABLE",
        "factor",
        1,
        "p_factor_unquoted_variable",
        "parser.py",
        144,
    ),
    (
        "factor -> LITERAL_NUMBER",
        "factor",
        1,
        "p_factor_literal_number",
        "parser.py",
        149,
    ),
    ("factor -> SUB expr", "factor", 2, "p_factor_negative_expr", "parser.py", 154),
    ("factor -> SUBSTEP", "factor", 1, "p_factor_substep", "parser.py", 159),
    ("factor -> LPAREN expr RPAREN", "factor", 3, "p_factor_expr", "parser.py", 164),
]
