��          �               L      M     n  )   �  2   �     �  *   �  -        I     O     d  @   u     �  	   �  ,   �  .   �  -   ,     Z     f  �  �  7        N  2   k  C   �  	   �  6   �  5   #     Y     ^     o  V   �  "   �       D     5   T  4   �  #   �  0   �   <p>{title} was optimized away<p> Aggregated over items Attribute '{attr.name}' has value {value} Attribute '{attr.name}' is in the range of {range} Constant price Demand date since {start} and before {end} Execution date since {start} and before {end} Price Price is not defined Procedure {name} Quantity is greater or equal to {min} and less that {upperbound} The value of %s Undefined Undefined because not branch matched in "%s" Undefined because the formula contained errors Undefined because the procedure was ill-typed Value of %s {title} was optimized away Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2024-02-18 18:07+0100
PO-Revision-Date: 2021-03-11 12:30-0500
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: fr_FR
Language-Team: fr_FR <LL@li.org>
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.14.0
 <p>La procédure « {title} » a été optimisée</p> Consolidation des résultats L'attribut « {attr.name} » a la valeur {value} L'attribut « {attr.name} » est compris dans la plage de {range} Prix fixe Date de la demande depuis le {start} et avant le {end} Date d'exécution depuis le {start} et avant le {end} Prix Prix non défini Procédure « {name} » La quantité est supérieure ou égale à {min} et inférieure à {limite supérieure} La valeur de l'attribut « %s » Non-défini Prix non défini car aucune condition n'était vraie dans « %s » Prix non défini car la formule contenait des erreurs Prix non défini car la procédure était mal typée La valeur de la variable « %s » La procédure « {title} » a été optimisée 