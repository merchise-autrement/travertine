��          �               \      ]     ~  )   �  2   �     �  *      -   +     Y     u     {     �  @   �     �  	   �  ,   �  .   )  -   X     �     �  �  �  2   =      p  2   �  9   �     �  @   
  >   K  &   �     �     �     �  H   �     8  
   U  <   `  ,   �  F   �       +   0   <p>{title} was optimized away<p> Aggregated over items Attribute '{attr.name}' has value {value} Attribute '{attr.name}' is in the range of {range} Constant price Demand date since {start} and before {end} Execution date since {start} and before {end} Matched from sub-procedures Price Price is not defined Procedure {name} Quantity is greater or equal to {min} and less that {upperbound} The value of %s Undefined Undefined because not branch matched in "%s" Undefined because the formula contained errors Undefined because the procedure was ill-typed Value of %s {title} was optimized away Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2024-02-18 18:07+0100
PO-Revision-Date: 2024-01-13 16:19+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.14.0
 <p>El procedimiento «{title}» fue optimizado</p> Consolidación de los resultados El atributo «{attr.name}» tiene el valor {valor} El atributo «{attr.name}» está en el intervalo {range} Precio fijo La fecha de la solicitud está entre {start} y {end} (excluído) La fecha de ejecución está entre {start} y {end} (excluído) Correspondiente a un sub-procedimiento Precio Le precio no ha sido definido Procedimiento «{name}» La cantidad solicitada es mayor igual que {min} y menor que {upperbound} El valor del atributo «%s» Indefinido Indefinido porque ninguna condición fue verdadera en «%s» Indefinido porque la fórmula tenía errores Indefinido porque el procedimiento está mal formado (tipo incorrecto) El valor de la variable «%s» El procedimiento «{title}» fue optimizado 