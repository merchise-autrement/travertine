#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# Copyright (c) Merchise Autrement [~º/~] and Contributors
# All rights reserved.
#
# This is free software; you can do what the LICENCE file allows you to.
#
from hypothesis import given
from hypothesis import strategies as st

from travertine import i18n
from travertine.procedures import ConstantProcedure
from travertine.tables import generate_tables


@given(
    st.sampled_from([
        ("es", "Precio"),
        ("fr_FR.UTF-8", "Prix"),
        ("de", "Price"),
    ])
)
def test_localized_table_headers(args):
    lang, result = args
    with i18n.locale(lang):
        table = next(generate_tables([("T", ConstantProcedure(10))]))
    assert table.columns_headers[-1] == result
