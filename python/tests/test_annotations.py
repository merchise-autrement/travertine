#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# Copyright (c) Merchise Autrement [~º/~] and Contributors
# All rights reserved.
#
# This is free software; you can do what the LICENCE file allows you to.
#
import typing as t
from dataclasses import dataclass

from travertine import ExternalObject, NullDemand, Program, UnitaryDemand


# Being able to import this module means with fixed #9:
# https://gitlab.merchise.org/mercurio-2018/travertine/-/issues/9
@dataclass
class _CacheNode:
    program: t.Optional[Program]
    obj: t.Optional[ExternalObject]
    demand: t.Union[None, UnitaryDemand, NullDemand]
