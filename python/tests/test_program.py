#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# Copyright (c) Merchise Autrement [~º/~] and Contributors
# All rights reserved.
#
# This is free software; you can do what the LICENCE file allows you to.
#
import datetime
from dataclasses import dataclass

import pytest
from hypothesis import given
from hypothesis import strategies as st
from hypothesis.stateful import run_state_machine_as_test

from travertine import ExternalObject, Program, UnitaryDemand, create_program
from travertine.predicates import (
    AttributeInRangePredicate,
    ExecutionPredicate,
    MatchesAttributePredicate,
    Otherwise,
    QuantityPredicate,
    ValidityPredicate,
)
from travertine.procedures import (
    BranchingProcedure,
    ConstantProcedure,
    FormulaProcedure,
    GetAttributeProcedure,
)
from travertine.testing.strategies.programs import ProgramMachine
from travertine.types import SimpleType, TypedAttribute, TypeName, Undefined

prices = st.floats(allow_infinity=False, allow_nan=False, min_value=0, max_value=200)
quantities = st.floats(allow_infinity=False, allow_nan=False, min_value=1, max_value=10)
dates = st.datetimes(
    min_value=datetime.datetime(2000, 1, 1),
    max_value=datetime.datetime(9000, 12, 31),
    # set timezone to avoid issue
    # https://github.com/HypothesisWorks/hypothesis/issues/2662
    timezones=st.just(datetime.timezone.utc),
    allow_imaginary=False,
).map(lambda d: d.replace(tzinfo=None, fold=0))


@given(prices)
def test_add_constant(value):
    program = Program()
    ConstantProcedure(value).add_to_travertine_program(program)
    assert program.execute(UnitaryDemand.default(), Undefined) == value


@given(st.lists(prices, min_size=2))
def test_add_constants_and_execute_last(values):
    program = Program(len(values))
    # I need to collect every procedure or Python could GC collect some
    # instances of ConstantProcedure and reuse an index.
    procedures = []
    for value in values:
        procedures.append(ConstantProcedure(value))
        procedures[-1].add_to_travertine_program(program)
    assert program.execute(UnitaryDemand.default(), Undefined) == values[-1]


@given(prices, prices.filter(bool), st.sampled_from(["+", "-", "*", "/"]))
def test_formula_procedure(v1, v2, operation):
    c1 = ConstantProcedure(v1)
    c2 = ConstantProcedure(v2)
    formula = FormulaProcedure(c1, c2, code=f"#1 {operation} #2")
    program = create_program(formula)
    if operation == "+":
        expected = v1 + v2
    elif operation == "-":
        expected = v1 - v2
    elif operation == "*":
        expected = v1 * v2
    elif operation == "/":
        expected = v1 / v2
    assert program.execute_many([UnitaryDemand.default(), UnitaryDemand.default()], Undefined) == [
        expected,
        expected,
    ]


typenames = st.from_type(TypeName).filter(lambda t: t != TypeName.UNKNOWN)
simpletypes = st.builds(SimpleType, typenames)
attrs = st.builds(TypedAttribute, st.sampled_from(["attr1", "attr2"]), simpletypes)


@given(attrs, prices)
def test_execute_getting_attr(attr, value):
    null = UnitaryDemand.default()
    demand = null.replace_attr(attr.name, value)
    assert demand.attr(attr.name) == value
    procedure = GetAttributeProcedure(attr)
    program = create_program(procedure)
    assert program.execute_many([null, demand], Undefined) == [Undefined, value]


def test_cant_add_twice_the_same_procedure():
    program = Program()
    procedure = ConstantProcedure(10)
    procedure.add_to_travertine_program(program)
    try:
        procedure.add_to_travertine_program(program)
    except ValueError:
        pass
    else:
        raise AssertionError("Should have raised a ValueError")


def test_arbitrary_programs_can_be_created():
    run_state_machine_as_test(ProgramMachine)


@dataclass(unsafe_hash=True)
class OtherObject:
    name: str
    id: int = 0

    def _to_travertine_external_object_(self) -> ExternalObject:
        return ExternalObject(f"urn:{self.__class__.__name__}:{self.name}", self.id)


objects = st.builds(OtherObject, st.text(min_size=1))


@given(attrs, objects, prices)
def test_arbitrary_types_with_good_player(attr, obj, price):
    proc = BranchingProcedure((MatchesAttributePredicate(attr, obj), ConstantProcedure(price)))
    program = create_program(proc)
    demand = UnitaryDemand.default()
    demand = demand.replace_attr(attr.name, obj._to_travertine_external_object_())
    assert program.execute(demand, Undefined) == price


@dataclass(unsafe_hash=True)
class BadPlayer:
    name: str

    def _to_travertine_external_object_(self):
        return OtherObject(self.name)


@given(attrs, st.builds(BadPlayer, st.text()))
def test_bad_players_raise_TypeError(attr, obj):
    proc = BranchingProcedure((MatchesAttributePredicate(attr, obj), ConstantProcedure(0)))
    with pytest.raises(TypeError):
        create_program(proc)


def test_regression_xhg2_issue1197():
    """Regression for test `#1197`__.

    Somehow there are branching procedures without branches or an 'otherwise'
    branch.

    __ https://gitlab.merchise.org/mercurio-2018/xhg2/-/issues/1197

    """
    proc = BranchingProcedure()
    program = create_program(proc)
    assert program.execute(UnitaryDemand.default(), Undefined) is Undefined

    proc = BranchingProcedure((Otherwise(), ConstantProcedure(100)))
    program = create_program(proc)
    assert program.execute(UnitaryDemand.default(), Undefined) == 100


@given(st.just(ValidityPredicate) | st.just(ExecutionPredicate), dates, dates)
def test_regression_issue5_dates(Predicate, d1, d2):
    """Regression for issue `#5`__ with ValidityPredicate and ExecutionPredicate.

    __ https://gitlab.merchise.org/mercurio-2018/travertine/-/issues/5

    The issue happens when branching procedures include predicates a boundless
    boundary.

    """
    if d1 and d2:
        d1, d2 = min(d1, d2), max(d1, d2)
    proc = BranchingProcedure(
        (Predicate(None, d1), ConstantProcedure(42)),
        (Predicate(d1, d2), ConstantProcedure(60)),
        (Predicate(d2, None), ConstantProcedure(69)),
    )
    create_program(proc)


@given(
    st.just(QuantityPredicate)
    | attrs.flatmap(lambda attr: st.just(lambda l1, l2: AttributeInRangePredicate(attr, l1, l2))),
    quantities,
    quantities,
)
def test_regression_issue5_floats(Predicate, f1, f2):
    """Regression for issue `#5`__ with ValidityPredicate and ExecutionPredicate.

    __ https://gitlab.merchise.org/mercurio-2018/travertine/-/issues/5

    The issue happens when branching procedures include predicates a boundless
    boundary.

    """
    proc = BranchingProcedure(
        (Predicate(None, f1), ConstantProcedure(42)),
        (Predicate(f1, f1 + f2), ConstantProcedure(60)),
        (Predicate(f1 + f2, None), ConstantProcedure(69)),
    )
    create_program(proc)
