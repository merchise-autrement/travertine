#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ---------------------------------------------------------------------
# Copyright (c) Merchise Autrement [~º/~] and Contributors
# All rights reserved.
#
# This is free software; you can do what the LICENCE file allows you to.
#
import typing as t
import unittest
from datetime import datetime, timedelta

import pytest
from hypothesis import HealthCheck, given, reproduce_failure, settings
from hypothesis import strategies as st
from hypothesis.stateful import rule, run_state_machine_as_test
from xotless.domains import EquivalenceSet as EqSet
from xotless.domains import IntervalSet, Range

from travertine.predicates import (
    AttributeInRangePredicate,
    ExecutionPredicate,
    MatchesAttributePredicate,
    Otherwise,
    QuantityPredicate,
    ValidityPredicate,
)
from travertine.procedures import (
    BranchingProcedure,
    ConstantProcedure,
    FormulaProcedure,
    GetAttributeProcedure,
    RoundProcedure,
)
from travertine.structs import Commodity, Demand, Request
from travertine.tables import (
    ATTR_FORMAT,
    MISSING_CELL,
    NULL_FORMAT,
    AttrFormatConfiguration,
    DemandData,
    ProgramDict,
    TableFormat,
    _get_table_avm_index,
    generate_demand_datas,
    generate_tables,
)
from travertine.testing.base import EMPTY_ENV, KaboomProcedure
from travertine.testing.strategies.programs import BasicProcedureMachine, ProcedureMachine
from travertine.testing.strategies.tables import table_formats
from travertine.testing.tables import (
    generate_full_tables,
    process_cell,
    process_table_rows,
    read_table_rows,
)
from travertine.types import AttributeLocator, SimpleType, TypedAttribute, Undefined

quantity_locator = AttributeLocator.of_request("quantity", float)
date_locator = AttributeLocator.of_demand("date", datetime)
occupation_locator = AttributeLocator.of_commodity("occupation", str)
occupation_attr = occupation_locator.attr
startdate_locator = AttributeLocator.of_commodity(
    "start_date",
    datetime,
    "Execution date",
)

CP = "Plan Continental"
MAP = "Media pensión"
TI = "Todo incluido"

regimen_attr_locator = AttributeLocator.of_commodity(
    TypedAttribute(
        "regimen",
        SimpleType.from_simple_selection([
            ("cp", CP, None),
            ("map", MAP, None),
            ("ai", TI, None),
        ]),
    )
)
regimen_attr = regimen_attr_locator.attr

TABLE_FORMAT = TableFormat(
    tables_conf=AttrFormatConfiguration(attrs=[quantity_locator], how=ATTR_FORMAT.BY_VALUE),
    columns_conf=[
        AttrFormatConfiguration(attrs=[occupation_locator, date_locator], how=ATTR_FORMAT.BY_VALUE),
        AttrFormatConfiguration(attrs=[startdate_locator], how=ATTR_FORMAT.BY_NAME),
    ],
)
TABLE_FORMAT_START_DATE_FIRST = TableFormat(
    tables_conf=AttrFormatConfiguration(attrs=[quantity_locator], how=ATTR_FORMAT.BY_VALUE),
    columns_conf=[
        AttrFormatConfiguration(attrs=[startdate_locator], how=ATTR_FORMAT.BY_NAME),
        AttrFormatConfiguration(attrs=[occupation_locator, date_locator], how=ATTR_FORMAT.BY_VALUE),
    ],
)

TABLE_FORMAT_REGIMEN_BY_VALUE = TableFormat(
    tables_conf=AttrFormatConfiguration(attrs=[], how=ATTR_FORMAT.BY_VALUE),
    columns_conf=[
        AttrFormatConfiguration(attrs=[regimen_attr_locator], how=ATTR_FORMAT.BY_VALUE),
    ],
)
TABLE_FORMAT_REGIMEN_AND_OCCUPATION_BY_VALUE = TableFormat(
    tables_conf=AttrFormatConfiguration(attrs=[], how=ATTR_FORMAT.BY_VALUE),
    columns_conf=[
        AttrFormatConfiguration(
            attrs=[regimen_attr_locator, occupation_locator], how=ATTR_FORMAT.BY_VALUE
        ),
    ],
)


ATTR_PAX = TypedAttribute("pax", SimpleType.from_python_type(int))
ATTR_PAX_LOCATOR = AttributeLocator.of_commodity(ATTR_PAX)

ATTR_QTY = TypedAttribute.from_typed_name("quantity", float)
ATTR_DATE = TypedAttribute.from_typed_name("date", datetime)
ATTR_START_DATE = TypedAttribute.from_typed_name("start_date", datetime)
ATTR_DURATION = TypedAttribute.from_typed_name("duration", timedelta)

#: Mark a test '@regression' to when it is a known bug that has not been fixed yet.
regression = pytest.mark.regression


class PriceTableMachine(ProcedureMachine):
    @rule(proc=ProcedureMachine.basic_procedures)
    def check_basic_procs_return_a_single_row_with_no_attrs(self, proc):
        if not isinstance(proc, GetAttributeProcedure) or not proc.sample_values:
            rows = list(
                generate_demand_datas(
                    proc.avm,
                    _get_table_avm_index(proc.avm, NULL_FORMAT),
                )
            )
            assert len(rows) == 1
            assert not rows[0].attrs

    @rule(proc=ProcedureMachine.validity_branching_procedures)
    def check_validity_branchings_generate_date_attr(self, proc):
        rows = list(generate_demand_datas(proc.avm, _get_table_avm_index(proc.avm, NULL_FORMAT)))
        for row in rows:
            assert (
                AttributeLocator.of_demand("date", datetime) in row.attr_locators
            ), f"Demand's date is not in {row.attrs_locators}"

    @rule(proc=ProcedureMachine.execution_branching_procedures)
    def check_exec_branchings_generate_start_date_attr(self, proc):
        rows = list(generate_demand_datas(proc.avm, _get_table_avm_index(proc.avm, NULL_FORMAT)))
        for row in rows:
            assert (
                AttributeLocator.of_commodity("start_date", datetime) in row.attr_locators
            ), f"Commodity's start_date is not in {row.attrs_locators}"

    @rule(proc=ProcedureMachine.procedures)
    def check_consistency_of_attrs(self, proc):
        datasets: t.List[DemandData] = list(
            generate_demand_datas(proc.avm, _get_table_avm_index(proc.avm, NULL_FORMAT))
        )
        if datasets:
            locators = datasets[0].attr_locators
            assert all(ds.attr_locators == locators for ds in datasets)

    @rule(proc=ProcedureMachine.procedures, fmt=table_formats)
    def check_we_can_compute_price_tables_for_any_format(self, proc, fmt):
        for table in generate_tables((("proc", proc),)):
            for _row in table.rows:
                pass

    @rule(proc=ProcedureMachine.procedures, fmt=table_formats)
    def check_table_attributes_in_the_index_are_in_the_avm(self, proc, fmt):
        avm = proc.avm
        index = _get_table_avm_index(avm, fmt)
        for attr in index:
            assert attr in avm, f"{attr} not in {avm}"


class PriceTableCaseIssue781Mixin:
    def _test_regression_issue781_table_generation_using_format(
        self, table_format, _use_rust_runtime=False
    ):
        first_step = BranchingProcedure(
            (QuantityPredicate(1, 4), ConstantProcedure(1)),
            (QuantityPredicate(4, 8), ConstantProcedure(2)),
            (QuantityPredicate(8, 14), ConstantProcedure(3)),
        )
        second_step = BranchingProcedure(
            # Prices from 2019-01-01 up to 2019-04-30
            (
                ValidityPredicate(datetime(2019, 1, 1), datetime(2019, 5, 1)),
                ConstantProcedure(42),
            ),
            # Prices from 2018-12-01 up to 2019-05-31
            (
                ValidityPredicate(datetime(2018, 12, 1), datetime(2019, 6, 1)),
                ConstantProcedure(69),
            ),
            (ValidityPredicate(datetime(2019, 2, 1), datetime(2019, 7, 1)), first_step),
            # Other dates.
            (Otherwise(), ConstantProcedure(51)),
        )
        tables = list(
            generate_full_tables(
                (("second_step", second_step),),
                table_format=table_format,
                _use_rust_runtime=_use_rust_runtime,
            )
        )
        table = tables[0]
        rows = table.rows
        headers = table.columns_headers
        self.assertEqual(
            headers,
            (
                (
                    Range.new_open_right(
                        datetime(2019, 1, 1, 0, 0), datetime(2019, 5, 1, 0, 0)
                    ).lift(),
                ),
                (
                    IntervalSet((
                        Range.new_open_right(
                            datetime(2018, 12, 1, 0, 0), datetime(2019, 1, 1, 0, 0)
                        ),
                        Range.new_open_right(
                            datetime(2019, 5, 1, 0, 0), datetime(2019, 6, 1, 0, 0)
                        ),
                    )),
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 6, 1, 0, 0), datetime(2019, 7, 1, 0, 0)
                    ).lift(),
                ),
            ),
        )
        self.assertEqual(rows, [(42, 69, 1)])


class PriceTableCase(unittest.TestCase, PriceTableCaseIssue781Mixin):
    maxDiff = None

    def test_price_tables_with_hypothesis_generated_programs(self):
        run_state_machine_as_test(
            PriceTableMachine,
            settings=settings(deadline=None, suppress_health_check=(HealthCheck.filter_too_much,)),
        )

    @given(st.floats(min_value=0, max_value=100.0, allow_infinity=False, allow_nan=False))
    def test_regression_duration_in_ranges_Rust(self, price):
        v1 = ConstantProcedure(price)
        first, middle, last = (
            timedelta(seconds=72000),
            timedelta(1, seconds=72000),
            timedelta(2, seconds=72000),
        )
        v2 = BranchingProcedure(
            (AttributeInRangePredicate(ATTR_DURATION, first, middle), v1),
            (AttributeInRangePredicate(ATTR_DURATION, middle, last), v1),
        )
        table = list(
            generate_full_tables(
                (("proc", v2),),
                table_format=NULL_FORMAT,
                _use_rust_runtime=True,
                lowerer=process_cell,
            )
        )[0]
        self.assertEqual(
            table.rows,
            process_table_rows([
                (Range.new_open_right(first, middle).lift(), price),
                (Range.new_open_right(middle, last).lift(), price),
            ]),
        )

    @given(fmt=table_formats)
    def test_regression_issue_tr_11(self, fmt):
        """Test Issue 11: TypeError: '<=' not supported between instances of 'int' and 'NoneType'"""
        v1 = FormulaProcedure(code='"var0"')
        v2 = BranchingProcedure((QuantityPredicate(0, 0), v1), (QuantityPredicate(0, 12), v1))
        self.check_we_rust_computes_the_same_price_table(v2, fmt)

    def check_we_rust_computes_the_same_price_table(self, proc, fmt):
        rust_tables = list(
            generate_full_tables(
                (("proc", proc),),
                table_format=fmt,
                _use_rust_runtime=True,
                lowerer=process_cell,
            )
        )
        python_tables = list(
            generate_full_tables((("proc", proc),), table_format=fmt, lowerer=process_cell)
        )
        self.assertEqual(rust_tables, python_tables)

    def _run_Rust_price_tables_with_hypothesis_generated_programs(
        self,
        version=None,
        blob=None,
    ):
        check = self.check_we_rust_computes_the_same_price_table

        if version and blob:
            decorator = reproduce_failure(version, blob)
        else:
            decorator = lambda c: c  # noqa

        @decorator
        class RustProcedureMachine(BasicProcedureMachine):
            @rule(proc=BasicProcedureMachine.procedures, fmt=table_formats)
            def check_we_rust_computes_the_same_price_table(self, proc, fmt):
                check(proc, fmt)

        run_state_machine_as_test(
            RustProcedureMachine,
            settings=settings(deadline=None, suppress_health_check=(HealthCheck.filter_too_much,)),
        )

    def test_Rust_price_tables_with_hypothesis_generated_programs(self):
        self._run_Rust_price_tables_with_hypothesis_generated_programs()

    def test_rounding_up_regression(self):
        proc = RoundProcedure(
            ConstantProcedure(title="Constant price", result=5.048709793414476e-29),
            2,
            "UP",
        )
        self.check_we_rust_computes_the_same_price_table(proc, NULL_FORMAT)

    def test_mixing_eqset_with_interval_sets(self):
        # Setup a program with two branching procedures.  The first one
        # (`procedure`) has a single branch with a predicate '1 <= pax < 3'.
        # This branch goes to a second branching procedure (`second_proc`)
        # with 4 branches each with with a condition of type 'pax == $val'.
        second_proc = BranchingProcedure(
            (MatchesAttributePredicate(ATTR_PAX, 1), ConstantProcedure(69 + 1)),
            (MatchesAttributePredicate(ATTR_PAX, 2), ConstantProcedure(69 + 2)),
            (MatchesAttributePredicate(ATTR_PAX, 3), ConstantProcedure(69 + 3)),
            (MatchesAttributePredicate(ATTR_PAX, 4), ConstantProcedure(69 + 4)),
        )
        procedure = BranchingProcedure((AttributeInRangePredicate(ATTR_PAX, 1, 3), second_proc))

        # Since branching procedures produce a *filtering* AVM, only the
        # values 1 and 2 are to be shown in the table.
        self.assertTrue(any(1 in domain for domain in procedure.avm[ATTR_PAX_LOCATOR]))
        self.assertTrue(any(2 in domain for domain in procedure.avm[ATTR_PAX_LOCATOR]))
        self.assertFalse(any(3 in domain for domain in procedure.avm[ATTR_PAX_LOCATOR]))
        self.assertFalse(any(4 in domain for domain in procedure.avm[ATTR_PAX_LOCATOR]))

    def test_regression_generation_of_None_for_quantity(self):
        v1 = ConstantProcedure(42)
        v2 = BranchingProcedure((QuantityPredicate(0, 0), v1), (QuantityPredicate(0, 1), v1))
        for demand_data in generate_demand_datas(v2.avm, _get_table_avm_index(v2.avm, NULL_FORMAT)):
            demand = demand_data.demand
            self.assertEqual(len(demand.requests), 1)
            self.assertIsNot(demand.requests[0].quantity, None)

    def test_table_generation_with_multiple_procedures(self):
        first_step = BranchingProcedure(
            (QuantityPredicate(1, 4), ConstantProcedure(1)),
            (QuantityPredicate(4, 8), ConstantProcedure(2)),
            (QuantityPredicate(8, 14), ConstantProcedure(3)),
        )
        second_step = BranchingProcedure(
            (
                ValidityPredicate(datetime(2018, 12, 1), datetime(2019, 6, 1)),
                ConstantProcedure(69),
            ),
            (
                ValidityPredicate(datetime(2019, 6, 1), datetime(2020, 7, 1)),
                ConstantProcedure(85),
            ),
        )
        third_step = BranchingProcedure(
            (QuantityPredicate(1, 6), ConstantProcedure(22)),
            (QuantityPredicate(6, 14), ConstantProcedure(96)),
        )
        tables = list(
            generate_full_tables((
                ("first_step", first_step),
                ("second_step", second_step),
                ("third_step", third_step),
            ))
        )
        self.assertEqual(tables[0].columns_headers, (ATTR_QTY, ATTR_DATE, "Price"))
        self.assertEqual(
            [row for table in tables for row in table.rows],
            [
                (Range.new_open_right(1, 4).lift(), MISSING_CELL, 1),
                (Range.new_open_right(4, 8).lift(), MISSING_CELL, 2),
                (Range.new_open_right(8, 14).lift(), MISSING_CELL, 3),
                (
                    MISSING_CELL,
                    Range.new_open_right(
                        datetime(2018, 12, 1, 0, 0), datetime(2019, 6, 1, 0, 0)
                    ).lift(),
                    69,
                ),
                (
                    MISSING_CELL,
                    Range.new_open_right(
                        datetime(2019, 6, 1, 0, 0), datetime(2020, 7, 1, 0, 0)
                    ).lift(),
                    85,
                ),
                (Range.new_open_right(1, 6).lift(), MISSING_CELL, 22),
                (Range.new_open_right(6, 14).lift(), MISSING_CELL, 96),
            ],
        )

    def test_regression_table_generation_with_ranged_procedures(self):
        first_step = BranchingProcedure(
            (QuantityPredicate(1, 4), ConstantProcedure(1)),
            (QuantityPredicate(4, 8), ConstantProcedure(2)),
            (QuantityPredicate(8, 14), ConstantProcedure(3)),
        )
        second_step = BranchingProcedure(
            # Prices from 2019-01-01 up to 2019-04-30
            (
                ValidityPredicate(datetime(2019, 1, 1), datetime(2019, 5, 1)),
                ConstantProcedure(42),
            ),
            # Prices from 2018-12-01 up to 2019-05-31
            (
                ValidityPredicate(datetime(2018, 12, 1), datetime(2019, 6, 1)),
                ConstantProcedure(69),
            ),
            (ValidityPredicate(datetime(2019, 2, 1), datetime(2019, 7, 1)), first_step),
            # Other dates.
            (Otherwise(), ConstantProcedure(51)),
        )
        tables = list(
            generate_full_tables(
                (("second_step", second_step),),
                TableFormat.from_columns(date_locator, quantity_locator),
            )
        )
        rows = tables[0].rows
        # Compare the set of rows because I'm not interested in the order in
        # which they are produced.  But I do need know the order in which
        # columns are in each row.
        self.assertEqual(
            set(rows),
            {
                (
                    Range.new_open_right(
                        datetime(2019, 1, 1, 0, 0), datetime(2019, 5, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(1, 4).lift(),
                    42,
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 1, 1, 0, 0), datetime(2019, 5, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(4, 8).lift(),
                    42,
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 1, 1, 0, 0), datetime(2019, 5, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(8, 14).lift(),
                    42,
                ),
                (
                    IntervalSet((
                        Range.new_open_right(
                            datetime(2018, 12, 1, 0, 0), datetime(2019, 1, 1, 0, 0)
                        ),
                        Range.new_open_right(
                            datetime(2019, 5, 1, 0, 0), datetime(2019, 6, 1, 0, 0)
                        ),
                    )),
                    Range.new_open_right(1, 4).lift(),
                    69,
                ),
                (
                    IntervalSet((
                        Range.new_open_right(
                            datetime(2018, 12, 1, 0, 0), datetime(2019, 1, 1, 0, 0)
                        ),
                        Range.new_open_right(
                            datetime(2019, 5, 1, 0, 0), datetime(2019, 6, 1, 0, 0)
                        ),
                    )),
                    Range.new_open_right(4, 8).lift(),
                    69,
                ),
                (
                    IntervalSet((
                        Range.new_open_right(
                            datetime(2018, 12, 1, 0, 0), datetime(2019, 1, 1, 0, 0)
                        ),
                        Range.new_open_right(
                            datetime(2019, 5, 1, 0, 0), datetime(2019, 6, 1, 0, 0)
                        ),
                    )),
                    Range.new_open_right(8, 14).lift(),
                    69,
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 6, 1, 0, 0), datetime(2019, 7, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(1, 4).lift(),
                    1,
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 6, 1, 0, 0), datetime(2019, 7, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(4, 8).lift(),
                    2,
                ),
                (
                    Range.new_open_right(
                        datetime(2019, 6, 1, 0, 0), datetime(2019, 7, 1, 0, 0)
                    ).lift(),
                    Range.new_open_right(8, 14).lift(),
                    3,
                ),
            },
        )

    def test_regression_issue781_table_generation_using_format_with_Python(self):
        self._test_regression_issue781_table_generation_using_format(TABLE_FORMAT)

    def test_regression_issue781_table_generation_using_format_with_Rust(self):
        self._test_regression_issue781_table_generation_using_format(
            TABLE_FORMAT, _use_rust_runtime=True
        )

    def _test_format_with_a_single_column_BY_VALUE(self, _use_rust_runtime=False):
        step = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), ConstantProcedure(1)),
            (MatchesAttributePredicate(occupation_attr, "DBL"), ConstantProcedure(2)),
            (MatchesAttributePredicate(occupation_attr, "TPL"), ConstantProcedure(3)),
        )
        tables = list(
            generate_full_tables(
                (("step", step),),
                table_format=TABLE_FORMAT,
                _use_rust_runtime=_use_rust_runtime,
            )
        )
        headers = tables[0].columns_headers
        rows = tables[0].rows
        self.assertEqual(headers, ((EqSet({"SGL"}),), (EqSet({"DBL"}),), (EqSet({"TPL"}),)))
        self.assertEqual(rows, [(1, 2, 3)])

    def test_format_with_a_single_column_BY_VALUE_in_Python(self):
        self._test_format_with_a_single_column_BY_VALUE()

    def test_format_with_a_single_column_BY_VALUE_in_Rust(self):
        self._test_format_with_a_single_column_BY_VALUE(_use_rust_runtime=True)

    def _test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME(self, _use_rust_runtime=False):
        step123 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), ConstantProcedure(1)),
            (MatchesAttributePredicate(occupation_attr, "DBL"), ConstantProcedure(2)),
            (MatchesAttributePredicate(occupation_attr, "TPL"), ConstantProcedure(3)),
        )
        step456 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), ConstantProcedure(4)),
            (MatchesAttributePredicate(occupation_attr, "DBL"), ConstantProcedure(5)),
            (MatchesAttributePredicate(occupation_attr, "TPL"), ConstantProcedure(6)),
        )
        exec_step = BranchingProcedure(
            # Prices from 2019-01-01 up to 2019-04-30
            (ExecutionPredicate(datetime(2019, 1, 1), datetime(2019, 5, 1)), step123),
            # Prices from 2018-12-01 up to 2019-05-31
            (ExecutionPredicate(datetime(2018, 12, 1), datetime(2019, 6, 1)), step456),
            (
                ExecutionPredicate(datetime(2019, 2, 1), datetime(2019, 7, 1)),
                ConstantProcedure(69),
            ),
            # Other dates.
            (Otherwise(), ConstantProcedure(42)),
        )
        tables = list(
            generate_full_tables(
                (("step", exec_step),),
                table_format=TABLE_FORMAT,
                _use_rust_runtime=_use_rust_runtime,
            )
        )
        headers = tables[0].columns_headers
        rows = tables[0].rows
        self.assertEqual(
            headers,
            ((EqSet({"SGL"}),), (EqSet({"DBL"}),), (EqSet({"TPL"}),), ATTR_START_DATE),
        )
        self.assertEqual(
            rows,
            [
                (
                    1,
                    2,
                    3,
                    Range.new_open_right(datetime(2019, 1, 1), datetime(2019, 5, 1)).lift(),
                ),
                (
                    4,
                    5,
                    6,
                    IntervalSet((
                        Range.new_open_right(datetime(2018, 12, 1), datetime(2019, 1, 1)),
                        Range.new_open_right(datetime(2019, 5, 1), datetime(2019, 6, 1)),
                    )),
                ),
                (
                    69,
                    69,
                    69,
                    Range.new_open_right(datetime(2019, 6, 1), datetime(2019, 7, 1)).lift(),
                ),
            ],
        )

    def test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_in_Python(self):
        self._test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME()

    def test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_in_Rust(self):
        self._test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME(_use_rust_runtime=True)

    def _test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_flipped(
        self, _use_rust_runtime=False, rust_runtime=None
    ):
        step123 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), ConstantProcedure(1)),
            (MatchesAttributePredicate(occupation_attr, "DBL"), ConstantProcedure(2)),
            (MatchesAttributePredicate(occupation_attr, "TPL"), ConstantProcedure(3)),
        )
        step456 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), ConstantProcedure(4)),
            (MatchesAttributePredicate(occupation_attr, "DBL"), ConstantProcedure(5)),
            (MatchesAttributePredicate(occupation_attr, "TPL"), ConstantProcedure(6)),
        )
        exec_step = BranchingProcedure(
            # Prices from 2019-01-01 up to 2019-04-30
            (ExecutionPredicate(datetime(2019, 1, 1), datetime(2019, 5, 1)), step123),
            # Prices from 2018-12-01 up to 2019-05-31
            (ExecutionPredicate(datetime(2018, 12, 1), datetime(2019, 6, 1)), step456),
            (
                ExecutionPredicate(datetime(2019, 2, 1), datetime(2019, 7, 1)),
                ConstantProcedure(69),
            ),
            # Other dates.
            (Otherwise(), ConstantProcedure(42)),
        )
        tables = list(
            generate_full_tables(
                (("step", exec_step),),
                table_format=TABLE_FORMAT_START_DATE_FIRST,
                _use_rust_runtime=_use_rust_runtime,
                rust_runtime=rust_runtime,
            )
        )
        headers = tables[0].columns_headers
        rows = tables[0].rows
        self.assertEqual(
            headers,
            (ATTR_START_DATE, (EqSet({"SGL"}),), (EqSet({"DBL"}),), (EqSet({"TPL"}),)),
        )
        self.assertEqual(
            rows,
            [
                (
                    Range.new_open_right(datetime(2019, 1, 1), datetime(2019, 5, 1)).lift(),
                    1,
                    2,
                    3,
                ),
                (
                    IntervalSet((
                        Range.new_open_right(datetime(2018, 12, 1), datetime(2019, 1, 1)),
                        Range.new_open_right(datetime(2019, 5, 1), datetime(2019, 6, 1)),
                    )),
                    4,
                    5,
                    6,
                ),
                (
                    Range.new_open_right(datetime(2019, 6, 1), datetime(2019, 7, 1)).lift(),
                    69,
                    69,
                    69,
                ),
            ],
        )
        return exec_step

    def test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_flipped_in_Python(
        self,
    ):
        self._test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_flipped()

    def test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_flipped_in_Rust(self):
        self._test_format_with_a_single_column_BY_VALUE_and_other_BY_NAME_flipped(
            _use_rust_runtime=True
        )

    def test_table_rows_are_lazy(self):
        # The TABLE_FORMAT will create three tables: one for each branch.  If
        # we ever try to read the rows of the second table, Kaboom!! but
        # reading the rows of the third table should not entail reading the
        # rows of the second.
        procedure = BranchingProcedure(
            (QuantityPredicate(1, 10), ConstantProcedure(69)),
            (QuantityPredicate(10, 20), KaboomProcedure()),
            (QuantityPredicate(20, 30), ConstantProcedure(42)),
        )
        top_procedure = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), procedure),
            (
                MatchesAttributePredicate(occupation_attr, "DBL"),
                FormulaProcedure(procedure, code="100 + #1"),
            ),
            (
                MatchesAttributePredicate(occupation_attr, "TPL"),
                FormulaProcedure(procedure, code="200 + #1"),
            ),
        )
        tables = generate_tables((("procedure", top_procedure),), table_format=TABLE_FORMAT)
        table = next(tables)
        rows = read_table_rows(table)
        self.assertEqual(rows, [(69, 169, 269)])
        next(tables)  # Skip dangerous second table.
        table = next(tables)
        rows = read_table_rows(table)
        self.assertEqual(rows, [(42, 142, 242)])

    def test_table_rows_are_lazy_but_do_work(self):
        procedure = BranchingProcedure(
            (QuantityPredicate(1, 10), ConstantProcedure(69)),
            (QuantityPredicate(10, 20), KaboomProcedure()),
            (QuantityPredicate(20, 30), ConstantProcedure(42)),
        )
        top_procedure = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), procedure),
            (
                MatchesAttributePredicate(occupation_attr, "DBL"),
                FormulaProcedure(procedure, code="100 + #1"),
            ),
            (
                MatchesAttributePredicate(occupation_attr, "TPL"),
                FormulaProcedure(procedure, code="200 + #1"),
            ),
        )
        tables = generate_tables((("procedure", top_procedure),), table_format=TABLE_FORMAT)
        table = next(tables)
        rows = read_table_rows(table)
        self.assertEqual(rows, [(69, 169, 269)])

        table = next(tables)
        with self.assertRaises(AssertionError):
            rows = read_table_rows(table)

        table = next(tables)
        rows = read_table_rows(table)
        self.assertEqual(rows, [(42, 142, 242)])

    def test_runtime_with_ProgramDict_remembers_procedures(self):
        proc = ConstantProcedure(45)
        runtime = ProgramDict()
        self.assertNotIn(proc, runtime)
        first = list(
            generate_full_tables(
                (("proc", proc),),
                _use_rust_runtime=True,
                rust_runtime=runtime,
                lowerer=process_cell,
            )
        )
        len_before_second = len(runtime)
        self.assertIn(proc, runtime)
        second = list(
            generate_full_tables(
                (("proc", proc),),
                _use_rust_runtime=True,
                rust_runtime=runtime,
                lowerer=process_cell,
            )
        )
        self.assertEqual(first, second)
        self.assertEqual(len(runtime), len_before_second)

    def test_empty_runtime_raises_KeyError(self):
        proc = ConstantProcedure(45)
        runtime = {}
        with self.assertRaises(KeyError):
            list(
                generate_full_tables(
                    (("proc", proc),),
                    _use_rust_runtime=True,
                    rust_runtime=runtime,
                    lowerer=process_cell,
                )
            )

    def test_table_with_simple_selection_in_cells(self):
        proc = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "cp"), ConstantProcedure(1.0)),
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(2.0)),
            (MatchesAttributePredicate(regimen_attr, "ai"), ConstantProcedure(3.0)),
        )
        tables = list(generate_full_tables((("proc", proc),), _use_rust_runtime=True))
        rows = tables[0].rows
        self.assertEqual(rows, [(CP, 1.0), (MAP, 2.0), (TI, 3.0)])

    def test_table_with_simple_selection_in_headers(self):
        proc = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "cp"), ConstantProcedure(1.0)),
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(2.0)),
            (MatchesAttributePredicate(regimen_attr, "ai"), ConstantProcedure(3.0)),
        )
        tables = list(
            generate_full_tables(
                (("proc", proc),),
                table_format=TABLE_FORMAT_REGIMEN_BY_VALUE,
                _use_rust_runtime=True,
            )
        )
        headers = tables[0].columns_headers
        rows = tables[0].rows
        self.assertEqual(headers, ((EqSet({CP}),), (EqSet({MAP}),), (EqSet({TI}),)))
        self.assertEqual(rows, [(1.0, 2.0, 3.0)])

    def test_regression_KeyError_issue_12(self):
        # The problem arises when we put two products in the same table, one of the
        # products have an attribute and the other doesn't, and we're formatting that
        # attribute BY_VALUE.
        proc2 = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "cp"), ConstantProcedure(1.0)),
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(2.0)),
            (MatchesAttributePredicate(regimen_attr, "ai"), ConstantProcedure(3.0)),
        )
        proc1 = BranchingProcedure(
            (QuantityPredicate(1, 10), ConstantProcedure(1)),
            (QuantityPredicate(10, 20), ConstantProcedure(10)),
        )
        tables = list(
            generate_full_tables(
                (("proc1", proc1), ("proc2", proc2)),
                table_format=TABLE_FORMAT_REGIMEN_BY_VALUE,
                _use_rust_runtime=True,
            )
        )
        self.assertEqual(
            tables[0].rows,
            [
                (1, 1, 1, Range.new_open_right(1, 10).lift()),
                (10, 10, 10, Range.new_open_right(10, 20).lift()),
            ],
        )
        self.assertEqual(
            tables[1].rows,
            [(1, 2, 3, MISSING_CELL)],
        )
        headers = tables[0].columns_headers
        self.assertEqual(
            headers,
            (
                (EqSet({CP}),),
                (EqSet({MAP}),),
                (EqSet({TI}),),
                quantity_locator.attr,
            ),
        )


class TestRegressionIssue1818(unittest.TestCase):
    maxDiff = None

    def setUp(self):
        super().setUp()
        proc11 = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "cp"), ConstantProcedure(4201)),
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(4202)),
        )
        proc12 = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "cp"), ConstantProcedure(4203)),
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(4204)),
        )
        self.proc1 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "SGL"), proc11),
            (MatchesAttributePredicate(occupation_attr, "DBL"), proc12),
        )
        proc21 = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(6901)),
            (MatchesAttributePredicate(regimen_attr, "ai"), ConstantProcedure(6902)),
        )
        proc22 = BranchingProcedure(
            (MatchesAttributePredicate(regimen_attr, "map"), ConstantProcedure(6903)),
            (MatchesAttributePredicate(regimen_attr, "ai"), ConstantProcedure(6904)),
        )
        self.proc2 = BranchingProcedure(
            (MatchesAttributePredicate(occupation_attr, "DBL"), proc21),
            (MatchesAttributePredicate(occupation_attr, "TPL"), proc22),
        )

    def test_issue1818_single_attr_BY_VALUE(self):
        tables = list(
            generate_full_tables(
                [("Proc 1", self.proc1), ("Proc 2", self.proc2)],
                table_format=TABLE_FORMAT_REGIMEN_BY_VALUE,
                _use_rust_runtime=True,
            )
        )
        headers = [table.columns_headers for table in tables]
        rows = [row for table in tables for row in table.rows]
        self.assertEqual(
            headers[0],
            (header(CP), header(MAP), header(TI), occupation_attr),
        )
        self.assertEqual(headers[0], headers[1])
        self.assertEqual(
            rows,
            [
                (4201.0, 4202.0, MISSING_CELL, "SGL"),
                (4203.0, 4204.0, MISSING_CELL, "DBL"),
                (MISSING_CELL, 6901.0, 6902.0, "DBL"),
                (MISSING_CELL, 6903.0, 6904.0, "TPL"),
            ],
        )

    def test_issue1818_many_attr_BY_VALUE(self):
        tables = list(
            generate_full_tables(
                [("Proc 1", self.proc1), ("Proc 2", self.proc2)],
                table_format=TABLE_FORMAT_REGIMEN_AND_OCCUPATION_BY_VALUE,
                _use_rust_runtime=True,
            )
        )
        headers = [table.columns_headers for table in tables]
        rows = [row for table in tables for row in table.rows]
        self.assertEqual(
            headers[0],
            (
                header(CP, "SGL"),
                header(CP, "DBL"),
                header(CP, "TPL"),
                header(MAP, "SGL"),
                header(MAP, "DBL"),
                header(MAP, "TPL"),
                header(TI, "SGL"),
                header(TI, "DBL"),
                header(TI, "TPL"),
            ),
        )
        self.assertEqual(headers[0], headers[1])
        self.assertEqual(
            rows,
            [
                (
                    4201.0,
                    4203.0,
                    MISSING_CELL,
                    4202.0,
                    4204.0,
                    MISSING_CELL,
                    MISSING_CELL,
                    MISSING_CELL,
                    MISSING_CELL,
                ),
                (
                    MISSING_CELL,
                    MISSING_CELL,
                    MISSING_CELL,
                    MISSING_CELL,
                    6901.0,
                    6903.0,
                    MISSING_CELL,
                    6902.0,
                    6904.0,
                ),
            ],
        )


class TestRegressionIssue2554(unittest.TestCase):
    # https://gitlab.merchise.org/mercurio-2018/xhg2/-/issues/2554
    maxDiff = None

    def setUp(self):
        super().setUp()
        attr = TypedAttribute.from_typed_name("attr1", int)
        self.proc = GetAttributeProcedure(attr, sample_values=[1, 2])

    def test_returns_the_value_of_attr1(self):
        commodity = Commodity(datetime(2024, 6, 5), timedelta(1)).replace(attr1=90)
        demand = Demand(datetime(2024, 5, 5), (Request(commodity, 1),))
        r = self.proc(demand, EMPTY_ENV)
        self.assertEqual(r.result, 90)

    def test_returns_the_undefined(self):
        commodity = Commodity(datetime(2024, 6, 5), timedelta(1))
        demand = Demand(datetime(2024, 5, 5), (Request(commodity, 1),))
        r = self.proc(demand, EMPTY_ENV)
        self.assertEqual(r.result, Undefined)

    def test_table_generates_all_samples_python(self):
        tables = list(generate_full_tables((("proc", self.proc),), _use_rust_runtime=False))
        rows = [row for table in tables for row in table.rows]
        self.assertEqual(rows, [(1, 1), (2, 2)])

    def test_table_generates_all_samples_rust(self):
        tables = list(generate_full_tables((("proc", self.proc),), _use_rust_runtime=True))
        rows = [row for table in tables for row in table.rows]
        self.assertEqual(rows, [(1, 1), (2, 2)])


def header(*values):
    return tuple(EqSet({v}) for v in values)
