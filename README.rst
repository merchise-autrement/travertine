=================================================
 Generating pricing tables from pricing programs
=================================================

Travertine tries to solve the problem of generating price tables fast enough
for our xhg2 project.  This Rust runtime **is not a replacement** of the
Python runtime because there are still types of procedures which are not
implemented in Rust.
