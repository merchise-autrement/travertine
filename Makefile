RYE_EXEC := rye run
PROJECT := travertine
SHELL := /bin/bash
PATH := $(HOME)/.rye/shims:$(PATH)

PYTHON_VERSION ?= 3.8

PYTHON_FILES := $(shell find python -type f -name '*.py')
RUST_FILES := $(shell find src -type f -name '*.rs')

USE_UV ?= true
REQUIRED_UV_VERSION ?= 0.2.2
REQUIRED_RYE_VERSION ?= 0.34.0
bootstrap:
	@INSTALLED_UV_VERSION=$$(uv --version 2>/dev/null | awk '{print $$2}' || echo "0.0.0"); \
    UV_VERSION=$$(printf '%s\n' "$(REQUIRED_UV_VERSION)" "$$INSTALLED_UV_VERSION" | sort -V | head -n1); \
	if [ "$$UV_VERSION" != "$(REQUIRED_UV_VERSION)" ]; then \
		curl -LsSf https://astral.sh/uv/install.sh | sh; \
	fi
	@INSTALLED_RYE_VERSION=$$(rye --version 2>/dev/null | head -n1 | awk '{print $$2}' || echo "0.0.0"); \
	DETECTED_RYE_VERSION=$$(printf '%s\n' "$(REQUIRED_RYE_VERSION)" "$$INSTALLED_RYE_VERSION" | sort -V | head -n1); \
	if [ "$$DETECTED_RYE_VERSION" != "$(REQUIRED_RYE_VERSION)" ]; then \
		rye self update || curl -sSf https://rye.astral.sh/get | RYE_INSTALL_OPTION="--yes" RYE_VERSION="$(REQUIRED_RY_VERSION)" bash; \
	fi
	@rye config --set-bool behavior.use-uv=$(USE_UV)
	@rye pin --relaxed $(PYTHON_VERSION)

install: bootstrap
	@rye sync -f
	@cp -f requirements-dev.lock requirements-dev-py$$(echo $(PYTHON_VERSION) | sed "s/\.//").lock
.PHONY: install

sync: bootstrap
	@rye sync --no-lock
.PHONY: sync

lock: bootstrap
ifdef update_all
	@rye sync --update-all
else
	@rye sync
endif
	@cp requirements-dev.lock requirements-dev-py$$(echo $(PYTHON_VERSION) | sed "s/\.//").lock
.PHONY: lock

update: bootstrap
	@$(MAKE) lock update_all=1
	@cargo update
.PHONY: update

build: sync
	@rye build
.PHONY: build


test: test-rust test-python
.PHONY: test

test-rust:
	@cargo test
.PHONY: test-rust

PYTEST_ARGS ?=
test-python:
	@pytest_workers_args=""; \
    if [ -n "$(PYTEST_WORKERS)" ]; then \
       pytest_workers_args="-n $(PYTEST_WORKERS)"; \
       if [ -n "$(PYTEST_MAXWORKERS)" ]; then \
          pytest_workers_args="$$pytest_workers_args --maxprocesses=$(PYTEST_MAXWORKERS)"; \
       fi \
    fi; \
    $(RYE_EXEC) pytest -v python --ff $(PYTEST_ARGS) $$pytest_workers_args
.PHONY: test-python

docs:
	SPHINXBUILD="$(RYE_EXEC) sphinx-build" make -C docs html
.PHONY: docs

doctest:
	SPHINXBUILD="$(RYE_EXEC) sphinx-build" make -C docs doctest SPHINXOPTS=-W
.PHONY: doctest

mypy:
	@$(RYE_EXEC) mypy -p travertine --config mypy.ini
.PHONY: python-shell

shell:
	@$(RYE_EXEC) ipython
.PHONY: python-shell


# Style checking

format: format-rust format-python
.PHONY: format

format-rust:
	@rustup component add clippy --toolchain stable 2> /dev/null
	@cargo fmt
.PHONY: format-rust

format-python:
	$(RYE_EXEC) ruff check --fix python/
	$(RYE_EXEC) ruff format python/
	$(RYE_EXEC) isort python/
.PHONY: format-python

lint-rust:
	@rustup component add rustfmt --toolchain stable 2> /dev/null
	@cargo clippy --all-features --all --tests -- -D clippy::all
.PHONY: lint-rust

lint-python:
	$(RYE_EXEC) ruff check python/
	$(RYE_EXEC) ruff format --check python/
	$(RYE_EXEC) isort --check python/
.PHONY: lint-python

lint: lint-python lint-rust
.PHONY: lint

# i18n
LOCALE_DIR := python/travertine/i18n/locale
POT_FILE := $(LOCALE_DIR)/$(PROJECT).pot
LANGUAGES ?= es en_US fr_FR

update-locales: $(POT_FILE)
	for lang in $(LANGUAGES); do \
		if [ -d $(LOCALE_DIR)/$$lang ]; then \
			TRAVERTINE_I18N_CLI=1 rye run pybabel update -D $(PROJECT) -l $$lang -d $(LOCALE_DIR) -i $(POT_FILE); \
		else \
			TRAVERTINE_I18N_CLI=1 rye run pybabel init -D $(PROJECT) -l $$lang -d $(LOCALE_DIR) -i $(POT_FILE); \
		fi; \
	done

.PHONY: update-locales

compile-locales:
	for lang in $(LANGUAGES); do \
		TRAVERTINE_I18N_CLI=1 rye run pybabel compile -D $(PROJECT) -l $$lang -d $(LOCALE_DIR); \
	done

.PHONY: compile-locales

$(POT_FILE): $(PYTHON_FILES) Makefile
	TRAVERTINE_I18N_CLI=1 rye run pybabel extract -c i18n -o $(POT_FILE) \
        --copyright-holder "Merchise Autrement [~º/~] and Contributors" \
        --project $(PROJECT) \
        $(PYTHON_FILES)


CADDY_SERVER_PORT ?= 9999
caddy: docs
	@docker run --rm -p $(CADDY_SERVER_PORT):$(CADDY_SERVER_PORT) \
         -v $(PWD)/docs/build/html:/var/www -it caddy \
         caddy file-server --browse --listen :$(CADDY_SERVER_PORT) --root /var/www
.PHONY: caddy

# Local Variables:
# indent-tabs-mode: t
# End:
