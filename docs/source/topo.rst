======================================================
 :mod:`travertine.topo` -- Topological sort of graphs
======================================================

.. automodule:: travertine.topo

.. autofunction:: topological_sort
