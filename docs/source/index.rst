.. travertine documentation master file, created by
   sphinx-quickstart on Sat May 30 21:35:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to travertine's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   problem
   api
   testing
   topo
   history
   ideas


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
