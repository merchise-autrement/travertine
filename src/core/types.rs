use std::fmt::{Debug, Display};
use std::{convert::TryInto, hash::Hash};

/// This types trait provides a way to abstract the actual types of the values
/// in demands and pricing programs.
///
pub trait TravertineTypes: Ord + Clone + Debug {
    type DateTime: Ord + Clone + Debug;
    type Duration: Ord + Clone + Debug;
    type AttrId: Ord + Clone + Debug + Display;
    type VariableName: Ord + Clone + Debug + Hash + Display;

    type CustomValue: Ord + Clone + Debug + TryInto<f64>;

    fn resolve_variable_name(s: &str) -> Self::VariableName;
}
