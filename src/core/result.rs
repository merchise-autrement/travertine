use std::ops::{Add, Div, Mul, Neg, Sub};

/// The user-facing result of running a pricing program.
#[derive(Clone, Debug)]
pub enum ProcessResult {
    /// Undefined is a special value a program may use to indicate that no
    /// price can be provided.  The optional String can carry a meaningful
    /// message to the user.
    ///
    /// The programmer of the pricing program, may choose to use procedure
    /// ReturnUndefined in some circumstances.
    ///
    /// The VM also returns Undefined automatically in several scenarios.
    Undefined(Option<String>),

    Value(f64),
}

impl ProcessResult {
    pub fn annotated_undefined<S>(s: S) -> Self
    where
        S: Into<String>,
    {
        Self::Undefined(Some(s.into()))
    }

    pub fn value(f: f64) -> Self {
        Self::Value(f)
    }

    /// Maps the ProcessResult to Result.
    pub fn map_result(self) -> Result<f64, Option<String>> {
        match self {
            Self::Value(x) => Ok(x),
            Self::Undefined(u) => Err(u),
        }
    }
}

impl Add for ProcessResult {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Self::Value(l), Self::Value(r)) => Self::Value(l + r),

            // The following are Undefined
            (Self::Value(_), _) => rhs,
            (_, _) => self,
        }
    }
}

impl Sub for ProcessResult {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Self::Value(l), Self::Value(r)) => Self::Value(l - r),

            // The following are Undefined
            (Self::Value(_), _) => rhs,
            (_, _) => self,
        }
    }
}

impl Mul for ProcessResult {
    type Output = Self;
    fn mul(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Self::Value(l), Self::Value(r)) => Self::Value(l * r),

            // The following are Undefined
            (Self::Value(_), _) => rhs,
            (_, _) => self,
        }
    }
}

impl Div for ProcessResult {
    type Output = Self;
    fn div(self, rhs: Self) -> Self::Output {
        match (&self, &rhs) {
            (Self::Value(l), Self::Value(r)) => Self::Value(l / r),

            // The following are Undefined
            (Self::Value(_), _) => rhs,
            (_, _) => self,
        }
    }
}

impl Neg for ProcessResult {
    type Output = Self;
    fn neg(self) -> Self::Output {
        match &self {
            Self::Value(x) => Self::Value(-x),
            _ => self,
        }
    }
}

impl PartialEq for ProcessResult {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            // All Undefined values are equal
            (ProcessResult::Undefined(_), ProcessResult::Undefined(_)) => true,

            // But Value values are only equal if they carry the same value.
            (ProcessResult::Value(x), ProcessResult::Value(y)) if x == y => true,

            _ => false,
        }
    }
}
