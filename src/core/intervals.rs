//! Implementation of all kinds of (copyable) intervals in a single struct.
//!
//! The struct [Interval] can be used to represent all kinds (open, half-open)
//! of ranges.  At the moment, most of the implementation associated to
//! [Interval] requires the boundary values to implement [Copy].
//!
//! An interval can be created from standard ranges structs using
//! [Interval::from_range].  [Interval] also implements [From] for all range
//! structs in [std::ops].
//!
//! # On partial orders
//!
//! We allow representing intervals of over [PartialOrd] and don't require
//! [Ord].  This is to allow representing intervals over floating point
//! numbers.  But then our implementation make comparison *total* by
//! considering otherwise uncomparable values to be [Ordering::Equal].
//!
//! This places a burden on users of [Interval] who need to ensure never using
//! with partial ordered types except for [f32] or [f64].  Types that
//! implement [Ord] are well behaved and won't cause any trouble.  But float
//! point numbers must be properly guarded against using NaN or infinities.

#![allow(dead_code)]

use crate::core::totalize::{totalized_eq, totalized_less_or_equal, Totalized};
use std::cmp::Ordering;
use std::hash::{Hash, Hasher};
use std::ops::{
    Bound, Bound::*, Range, RangeBounds, RangeFrom, RangeFull, RangeInclusive, RangeTo,
    RangeToInclusive,
};

/// An interval over a partially ordered type.
#[derive(Clone, Copy, Debug)]
pub struct Interval<T> {
    lowerbound: Bound<T>,
    upperbound: Bound<T>,
}

/// A collection of non-overlapping intervals.
///
/// [IntervalSet] represent sets of a type T efficiently by creating
/// non-overlapping [intervals](Interval).  You can compute the difference,
/// union, test for membership, etc.
#[derive(Clone, Debug)]
pub struct IntervalSet<T: PartialOrd + Clone + Copy> {
    // TODO: Implement using an interval tree.
    nodes: Vec<Interval<T>>,
}

/// The four kinds of intervals
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum IntervalKind {
    /// Open intervals are those excluding both ends.  Notice that the
    /// [RangeFull] is always open.
    Open,

    /// Closed intervals are those including both ends.
    Closed,

    /// Open-Right intervals are those including its lower boundary and
    /// excluding the upper boundary.
    OpenRight,

    /// Open-Left intervals are those excluding its lower boundary and
    /// including the upper boundary.
    OpenLeft,
}

impl<T: PartialEq + PartialOrd + Copy> Interval<T> {
    /// Create a new valid Interval from the given bounds.
    fn from_range<R: RangeBounds<T>>(r: R) -> Self {
        let lowerbound: Bound<T> = match r.start_bound() {
            Included(x) => Included(*x),
            Excluded(x) => Excluded(*x),
            Unbounded => Unbounded,
        };
        let upperbound: Bound<T> = match r.end_bound() {
            Included(x) => Included(*x),
            Excluded(x) => Excluded(*x),
            Unbounded => Unbounded,
        };
        Interval {
            lowerbound,
            upperbound,
        }
    }

    /// Create a new valid Interval from the given bounds.
    ///
    /// All invalid or empty bounds are normalized to an empty [Interval] with
    /// Excluded bounds in both ends.
    pub fn new(lowerbound: Bound<T>, upperbound: Bound<T>) -> Self {
        match (lowerbound, upperbound) {
            (Included(l), Included(r)) if l > r => Interval {
                lowerbound: Excluded(r),
                upperbound: Excluded(r),
            },
            (Included(l), Excluded(r))
            | (Excluded(l), Included(r))
            | (Excluded(l), Excluded(r))
                if l >= r =>
            {
                Interval {
                    lowerbound: Excluded(r),
                    upperbound: Excluded(r),
                }
            }

            _ => Interval {
                lowerbound,
                upperbound,
            },
        }
    }

    /// Create an open Interval
    pub fn new_open(l: Option<T>, r: Option<T>) -> Self {
        let lower = match l {
            Some(a) => Excluded(a),
            None => Unbounded,
        };
        let upper = match r {
            Some(a) => Excluded(a),
            None => Unbounded,
        };
        Interval::new(lower, upper)
    }

    /// Create an closed Interval.
    ///
    /// Note that `[l, r]` is not valid (i.e `l > r`) we return a empty
    /// interval which is always open.
    pub fn new_closed(l: T, r: T) -> Self {
        Interval::new(Included(l), Excluded(r))
    }

    /// Create an open-right Interval.
    pub fn new_open_right(l: T, r: Option<T>) -> Self {
        let upper = match r {
            Some(a) => Excluded(a),
            None => Unbounded,
        };
        Interval::new(Included(l), upper)
    }

    /// Create an open-left Interval.
    pub fn new_open_left(l: Option<T>, r: T) -> Self {
        let lower = match l {
            Some(a) => Excluded(a),
            None => Unbounded,
        };
        Interval::new(lower, Included(r))
    }

    /// Return the [kind](IntervalKind) of the interval.
    pub fn kind(&self) -> IntervalKind {
        match (self.lowerbound, self.upperbound) {
            (Included(_), Included(_)) => IntervalKind::Closed,

            (Excluded(_), Excluded(_))
            | (Excluded(_), Unbounded)
            | (Unbounded, Excluded(_))
            | (Unbounded, Unbounded) => IntervalKind::Open,

            (Included(_), Excluded(_)) | (Included(_), Unbounded) => IntervalKind::OpenRight,

            (Excluded(_), Included(_)) | (Unbounded, Included(_)) => IntervalKind::OpenRight,
        }
    }

    /// Return a member of the interval if it's not empty (and not
    /// [open](IntervalKind.Open))
    pub fn get_sample(&self) -> Option<T> {
        if !self.is_empty() {
            match (self.lowerbound, self.upperbound) {
                (Included(l), _) | (_, Included(l)) => Some(l),
                _ => None,
            }
        } else {
            None
        }
    }

    /// Return true if the interval has no members.
    pub fn is_empty(&self) -> bool {
        match (self.lowerbound, self.upperbound) {
            (Included(l), Excluded(r)) if l >= r => true,
            (Excluded(l), Included(r)) if l >= r => true,
            (Excluded(l), Excluded(r)) if l >= r => true,
            _ => false,
        }
    }

    /// Test if `x` is a member of the interval.
    pub fn contains(&self, x: T) -> bool {
        match (self.lowerbound, self.upperbound) {
            (Unbounded, Unbounded) => true,
            (Unbounded, Excluded(r)) if x < r => true,
            (Unbounded, Included(r)) if x <= r => true,
            (Excluded(l), Unbounded) if l < x => true,
            (Included(l), Unbounded) if l <= x => true,

            (Included(l), Included(r)) if l <= x && x <= r => true,
            (Included(l), Excluded(r)) if l <= x && x < r => true,

            (Excluded(l), Included(r)) if l < x && x <= r => true,
            (Excluded(l), Excluded(r)) if l < x && x < r => true,

            _ => false,
        }
    }

    /// Compute the intersection of two intervals
    ///
    /// The intersection of two intervals is an interval which contains
    /// members which are members of both.
    pub fn intersect(&self, other: Interval<T>) -> Interval<T> {
        let lower = max_bound(self.lowerbound, other.lowerbound);
        let upper = min_bound(self.upperbound, other.upperbound);
        Interval::new(lower, upper)
    }

    /// Compute the difference of two intervals
    ///
    /// The result is a pair of intervals which (when considered together)
    /// have all the values of `self` that are not in `other`.  We need to
    /// return two intervals, because the difference is not necessarily a
    /// [Interval], but it can be captured by the union of two possibly empty
    /// intervals.
    ///
    /// We don't return empty intervals, instead we return None.  We can
    /// return `(None, Some(interval))` which help identify that `other` was
    /// "at the left" of `self`.
    pub fn diff(&self, other: Interval<T>) -> (Option<Interval<T>>, Option<Interval<T>>) {
        let other = self.intersect(other);
        if self.eq(&other) {
            (None, None)
        } else if !other.is_empty() {
            assert!(self.gt(&other));
            let before: Option<Interval<T>> = {
                let before = Interval::new(self.lowerbound, invert_boundary(other.lowerbound));
                if !before.is_empty() {
                    Some(before)
                } else {
                    None
                }
            };
            let after: Option<Interval<T>> = {
                let after = Interval::new(invert_boundary(other.upperbound), self.upperbound);
                if !after.is_empty() {
                    Some(after)
                } else {
                    None
                }
            };
            (before, after)
        } else {
            // So, `self` and `other` are disjoint and we must return `self`,
            // but other can be *at the right* of self, or at its left.  We
            // will return (None, self) if other is at the left of self, and
            // (self, None) if it is at its right.
            let upper = match other.upperbound {
                Included(x) | Excluded(x) => x,
                _ => panic!("There's an error in either intersect or diff!"),
            };
            let lower = match self.lowerbound {
                Included(x) | Excluded(x) => x,
                _ => panic!("There's an error in either intersect or diff!"),
            };
            if upper <= lower {
                (None, Some(*self))
            } else {
                (Some(*self), None)
            }
        }
    }

    /// Return true if both ends are not Unbounded.
    pub fn bound(&self) -> bool {
        !matches!(
            (self.lowerbound, self.upperbound),
            (Unbounded, _) | (_, Unbounded)
        )
    }

    pub fn join<R>(ranges: R) -> IntervalSet<T>
    where
        R: IntoIterator<Item = Interval<T>>,
    {
        let mut nodes: Vec<Interval<T>> = ranges.into_iter().collect();
        nodes.sort_by_key(|r| {
            let ordbound: OrdBound<_> = r.lowerbound.into();
            ordbound.lower_sort_key()
        });
        IntervalSet { nodes }
    }
}

impl<T: PartialOrd + Copy> PartialEq for Interval<T> {
    fn eq(&self, other: &Self) -> bool {
        if self.is_empty() {
            other.is_empty()
        } else {
            self.lowerbound == other.lowerbound && self.upperbound == other.upperbound
        }
    }
}

impl<T: PartialOrd + Copy> PartialOrd for Interval<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if self.eq(other) {
            Some(Ordering::Equal)
        } else if self.eq(&self.intersect(*other)) {
            Some(Ordering::Less)
        } else if other.eq(&self.intersect(*other)) {
            Some(Ordering::Greater)
        } else {
            None
        }
    }
}

impl<T: Hash> Hash for Interval<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        state.write(b"Interval<T>");
        self.lowerbound.hash(state);
        self.upperbound.hash(state);
    }
}

macro_rules! implement_from_range {
    ($t: ident) => {
        impl<T> From<$t<T>> for Interval<T>
        where
            T: Copy + PartialEq + PartialOrd,
        {
            fn from(r: $t<T>) -> Self {
                Interval::from_range(r)
            }
        }
    };
}

implement_from_range!(Range);
implement_from_range!(RangeTo);
implement_from_range!(RangeInclusive);
implement_from_range!(RangeToInclusive);
implement_from_range!(RangeFrom);

impl<T> From<RangeFull> for Interval<T>
where
    T: Copy + PartialEq + PartialOrd,
{
    fn from(r: RangeFull) -> Self {
        Interval::from_range(r)
    }
}

pub(crate) struct OrdBound<T> {
    bound: Bound<T>,
}

impl<T: PartialOrd + Copy> OrdBound<T> {
    #[inline]
    fn lower_sort_key(&self) -> (i8, Option<Totalized<T>>) {
        match self.bound {
            Unbounded => (-1, None),
            Included(item) => (0, Some(item.into())),
            Excluded(item) => (0, Some(item.into())),
        }
    }

    #[inline]
    fn upper_sort_key(&self) -> (i8, Option<Totalized<T>>) {
        match self.bound {
            Unbounded => (1, None),
            Included(item) => (0, Some(item.into())),
            Excluded(item) => (0, Some(item.into())),
        }
    }
}

impl<T: PartialOrd + Copy> From<Bound<T>> for OrdBound<T> {
    fn from(bound: Bound<T>) -> Self {
        OrdBound { bound }
    }
}

#[inline]
fn max_bound<T: PartialOrd + Copy>(b1: Bound<T>, b2: Bound<T>) -> Bound<T> {
    match (b1, b2) {
        (Unbounded, _) => b1,
        (_, Unbounded) => b2,

        (Included(l), Excluded(r)) if totalized_eq(&l, &r) => b2,
        (Excluded(l), Included(r)) if totalized_eq(&l, &r) => b1,

        (Included(l), Included(r)) if totalized_less_or_equal(&l, &r) => b2,
        (Excluded(l), Excluded(r)) if totalized_less_or_equal(&l, &r) => b2,
        _ => b1,
    }
}

#[inline]
fn min_bound<T: PartialOrd + Copy>(b1: Bound<T>, b2: Bound<T>) -> Bound<T> {
    match (b1, b2) {
        (Unbounded, _) => b1,
        (_, Unbounded) => b2,

        (Included(l), Excluded(r)) if totalized_eq(&l, &r) => b2,
        (Excluded(l), Included(r)) if totalized_eq(&l, &r) => b1,

        (Included(l), Included(r)) if totalized_less_or_equal(&l, &r) => b1,
        (Excluded(l), Excluded(r)) if totalized_less_or_equal(&l, &r) => b1,
        _ => b2,
    }
}

/// Inverts the boundary type.
///
/// Unbounded is left unchanged, Included becomes Excluded and Excluded
/// becomes Included.
#[inline]
fn invert_boundary<T: Copy>(b: Bound<T>) -> Bound<T> {
    match b {
        Included(x) => Excluded(x),
        Excluded(x) => Included(x),
        Unbounded => Unbounded,
    }
}

#[cfg(test)]
mod basic_tests {
    use super::*;
    use proptest::prelude::*;

    #[test]
    fn from_range() {
        assert_eq!(
            Interval::from(1..10),
            Interval::new(Included(1), Excluded(10))
        );
        assert_eq!(
            Interval::from(1..=10),
            Interval::new(Included(1), Included(10))
        );
        assert_eq!(
            Interval::from(..=10),
            Interval::new(Unbounded, Included(10))
        );
        assert_eq!(Interval::from(..10), Interval::new(Unbounded, Excluded(10)));
        assert_eq!(Interval::from(1..), Interval::new(Included(1), Unbounded));
        let x: Interval<u8> = Interval::from(..);
        assert_eq!(x, Interval::new(Unbounded, Unbounded));
    }

    proptest! {
        #[test]
        fn emptyness_of_i32(a in -100_000..100_100i32, b in -100_000..100_100i32) {
            let interval = Interval::from_range(a..b);
            if a >= b {
                prop_assert!(interval.is_empty());
            } else {
                prop_assert!(!interval.is_empty());
            }

            let interval = Interval::from_range(a..a);
            prop_assert!(interval.is_empty());

            let interval = Interval::from_range(a..=a);
            prop_assert!(!interval.is_empty());

            let interval = Interval::new(Excluded(a), Included(a));
            prop_assert!(interval.is_empty());
        }

        #[test]
        fn intersection_with_myself(a in -100_000..100_101i32, b in -100_000..100_101i32) {
            let range = Interval::from_range(a..b);
            let intersection = range.intersect(range);
            prop_assert_eq!(range, intersection);
        }

        #[test]
        fn diff_of_inner_interval(outer_lower in -100_000..-10_000i32,
                                  inner_lower in -10_000..0i32,
                                  inner_upper in 1..10_000i32,
                                  outer_upper in 10_000..100_000i32) {
            let outer_interval = Interval::from_range(outer_lower..outer_upper);
            let inner_interval = Interval::from_range(inner_lower..inner_upper);
            let (before, after) = outer_interval.diff(inner_interval);
            prop_assert_ne!(before, None);
            prop_assert_ne!(after, None);
        }

        #[test]
        fn diff_of_outer_interval(outer_lower in -100_000..-10_000i32,
                                  inner_lower in -10_000..0i32,
                                  inner_upper in 1..10_000i32,
                                  outer_upper in 10_000..100_000i32) {
            let outer_interval = Interval::from_range(outer_lower..outer_upper);
            let inner_interval = Interval::from_range(inner_lower..inner_upper);
            let (before, after) = inner_interval.diff(outer_interval);
            prop_assert_eq!(before, None);
            prop_assert_eq!(after, None);
        }
    }
}
