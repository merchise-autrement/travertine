use std::str::FromStr;
use crate::core::formulae::ast::{AST, BinaryOperator};

grammar;

pub Formula = Expression;

/// Tier.
///
/// A left-recursive (associative) tier of binary operators.
Tier<Op, NextTier>: AST = {
    <l:Tier<Op, NextTier>> <o:Op> <r:NextTier> => AST::BinaryOperation(
        o,
        Box::new(l),
        Box::new(r)
    ),
    NextTier
};

Expression: AST = Tier<ExprOp, Term>;
Term: AST = Tier<TermOp, Factor>;

ExprOp: BinaryOperator = {
    "+" => BinaryOperator::Add,
    "-" => BinaryOperator::Sub,
}

TermOp: BinaryOperator = {
    "*" => BinaryOperator::Mult,
    "/" => BinaryOperator::Div,
}

Factor: AST = {
    Variable,
    Number,
    Substep,
    Negation,
    "(" <e:Expression> ")" => e
}

Negation: AST = {
    "-" <e:Factor> => AST::negate(e)
}

Variable: AST = {
    <s:r"[a-zA-Z][a-zA-Z_0-9]*"> => AST::new_var(&s),
    <s:r"'[^']+'"> => AST::new_var(&s[1..s.len()-1]),
    <s:r##""[^"]+""##> => AST::new_var(&s[1..s.len()-1])
}

Number: AST = {
    <n:r"\d+(\.\d+)?([eE][\-\+]?\d+)?"> => AST::new_literal(f64::from_str(n).unwrap()),
}

Substep: AST = SubstepIndex => AST::new_substep(<>);
SubstepIndex: usize = {
    <s:r"#[1-9][0-9]*"> => usize::from_str(&s[1..]).unwrap(),

    /// This is a deprecated variant of the syntax ($1, $2, ...) but we allow
    /// it.
    <s:r"\$[1-9][0-9]*"> => usize::from_str(&s[1..]).unwrap(),
}


// Local Variables:
// mode: rust
// End:
