use crate::core::types::TravertineTypes;

use std::marker::PhantomData;

#[derive(Clone, Debug)]
pub struct Formula<C: TravertineTypes> {
    code: String,
    pub(crate) ast: AST,
    phantom: PhantomData<C>,
}

impl<C: TravertineTypes> std::fmt::Display for Formula<C> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        writeln!(f, "Formula{{code: {}}}", self.code)
    }
}

impl<C: TravertineTypes> Formula<C> {
    pub fn from_code(code: &str) -> Result<Self, String> {
        use super::grammar::FormulaParser;
        let ast = FormulaParser::new()
            .parse(code)
            .map_err(|e| format!("{:?}", e))?;
        Ok(Self {
            code: code.to_string(),
            ast,
            phantom: PhantomData,
        })
    }

    /// Return the max substep index in the AST.  If no substeps are in the
    /// AST, return 0.
    pub fn max_substep_index(&self) -> usize {
        self.ast.max_substep_index()
    }
}

#[allow(clippy::upper_case_acronyms)]
#[derive(Clone, PartialEq, Debug)]
pub enum AST {
    Variable(String),
    Substep(usize),
    LiteralNumber(f64),
    BinaryOperation(BinaryOperator, Box<AST>, Box<AST>),
    Negation(Box<AST>),
}

impl AST {
    pub fn new_var<S>(name: S) -> AST
    where
        S: ToString,
    {
        AST::Variable(name.to_string())
    }

    pub fn new_substep(id: usize) -> AST {
        AST::Substep(id)
    }

    pub fn new_literal(num: f64) -> AST {
        AST::LiteralNumber(num)
    }

    /// Return the max substep index in the AST.  If no substeps are in the
    /// AST, return 0.
    fn max_substep_index(&self) -> usize {
        match self {
            Self::Substep(u) => *u,
            Self::Negation(b) => (*b).max_substep_index(),
            Self::BinaryOperation(_, b1, b2) => {
                let u1 = (*b1).max_substep_index();
                let u2 = (*b2).max_substep_index();
                if u1 < u2 {
                    u2
                } else {
                    u1
                }
            }
            _ => 0,
        }
    }

    /// Negates an expression.
    ///
    /// If the expression is also a Negation, we simply unwrap the underlying
    /// expression.  If it's LiteralNumber, we return a LiteralNumber negating
    /// its content.  Any other type of expression is simply wrapped inside a
    /// Negation node.
    ///
    /// As with all constructors, we take ownership of the given expression.
    pub fn negate(expr: AST) -> AST {
        match expr {
            AST::Negation(x) => *x,
            AST::LiteralNumber(x) => AST::LiteralNumber(-x),
            e => AST::Negation(Box::new(e)),
        }
    }

    /// Creates the node representing the sum of two sub-expressions.
    #[cfg(test)]
    pub fn sum(left: AST, right: AST) -> AST {
        AST::BinaryOperation(BinaryOperator::Add, Box::new(left), Box::new(right))
    }

    /// Creates the node representing the multiplication of two
    /// sub-expressions.
    #[cfg(test)]
    pub fn mult(left: AST, right: AST) -> AST {
        AST::BinaryOperation(BinaryOperator::Mult, Box::new(left), Box::new(right))
    }

    /// Creates the node representing the division of two sub-expressions.
    #[cfg(test)]
    pub fn div(left: AST, right: AST) -> AST {
        AST::BinaryOperation(BinaryOperator::Div, Box::new(left), Box::new(right))
    }

    /// Creates the node representing the subtraction of two sub-expressions.
    #[cfg(test)]
    pub fn sub(left: AST, right: AST) -> AST {
        AST::BinaryOperation(BinaryOperator::Sub, Box::new(left), Box::new(right))
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum BinaryOperator {
    Add,
    Sub,
    Mult,
    Div,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn building_ast_from_parts() {
        let var_a = AST::new_var("a");
        assert_eq!(format!("{:?}", var_a), "Variable(\"a\")");

        let substep_1 = AST::new_substep(1);
        assert_eq!(format!("{:?}", substep_1), "Substep(1)");

        let f1 = AST::negate(AST::sum(var_a, substep_1));
        assert_eq!(
            format!("{:?}", f1),
            "Negation(BinaryOperation(Add, Variable(\"a\"), Substep(1)))"
        );
    }

    #[test]
    fn building_double_negation_is_elided() {
        let var_a = AST::new_var("a");
        let substep_1 = AST::new_substep(1);
        assert_eq!(
            AST::negate(AST::negate(AST::sum(var_a.clone(), substep_1.clone()))),
            AST::sum(var_a, substep_1)
        );
    }

    #[test]
    fn building_negation_is_internalized_in_numbers() {
        assert_eq!(AST::negate(AST::new_literal(1.0)), AST::new_literal(-1.0));
    }
}
