use lalrpop_util::lalrpop_mod;

pub(crate) mod ast;

lalrpop_mod!(
    #[rustfmt::skip]
    #[allow(clippy::all)]

    pub(crate) grammar,
    "/core/formulae/grammar.rs"
);

#[cfg(test)]
mod tests {
    use super::ast::*;
    use super::grammar::FormulaParser;

    #[test]
    fn parsing_basic_formulae() {
        assert_eq!(FormulaParser::new().parse("#1").unwrap(), AST::Substep(1));
        assert_eq!(FormulaParser::new().parse("$1").unwrap(), AST::Substep(1));

        assert_eq!(
            FormulaParser::new().parse("'variable'").unwrap(),
            AST::Variable("variable".to_string())
        );
        assert_eq!(
            FormulaParser::new().parse("variable").unwrap(),
            AST::Variable("variable".to_string())
        );
        assert_eq!(
            FormulaParser::new().parse("1.878").unwrap(),
            AST::LiteralNumber(1.878)
        );
    }

    #[test]
    fn test_no_substep_zero() {
        assert!(FormulaParser::new().parse("#0").is_err());
        assert!(FormulaParser::new().parse("#01").is_err());
    }

    #[test]
    fn test_regression_no_spaces_before_number() {
        assert_eq!(
            FormulaParser::new().parse("#1-6").unwrap(),
            AST::BinaryOperation(
                BinaryOperator::Sub,
                Box::new(AST::Substep(1)),
                Box::new(AST::LiteralNumber(6.0))
            )
        );
    }

    #[test]
    fn test_avoid_negation_on_literal_numbers() {
        assert_eq!(
            FormulaParser::new().parse("---6").unwrap(),
            AST::LiteralNumber(-6.0)
        );

        assert_eq!(
            FormulaParser::new().parse("#1 - ---6").unwrap(),
            AST::BinaryOperation(
                BinaryOperator::Sub,
                Box::new(AST::Substep(1)),
                Box::new(AST::LiteralNumber(-6.0))
            )
        );
    }

    #[test]
    fn test_left_associativity_of_all_binary_operators() {
        assert_eq!(
            FormulaParser::new().parse("#1 + #2 + #3").unwrap(),
            FormulaParser::new().parse("(#1 + #2) + #3").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 + #2 + #3").unwrap(),
            AST::sum(AST::sum(AST::Substep(1), AST::Substep(2)), AST::Substep(3))
        );

        assert_eq!(
            FormulaParser::new().parse("#1 - #2 - #3").unwrap(),
            FormulaParser::new().parse("(#1 - #2) - #3").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 - #2 - #3").unwrap(),
            AST::sub(AST::sub(AST::Substep(1), AST::Substep(2)), AST::Substep(3))
        );

        assert_eq!(
            FormulaParser::new().parse("#1 - #2 + #3").unwrap(),
            FormulaParser::new().parse("(#1 - #2) + #3").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 - #2 + #3").unwrap(),
            AST::sum(AST::sub(AST::Substep(1), AST::Substep(2)), AST::Substep(3))
        );

        assert_eq!(
            FormulaParser::new().parse("#1 * #2 * #3").unwrap(),
            FormulaParser::new().parse("(#1 * #2) * #3").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 * #2 * #3").unwrap(),
            AST::mult(AST::mult(AST::Substep(1), AST::Substep(2)), AST::Substep(3))
        );

        assert_eq!(
            FormulaParser::new().parse("#1 / #2 / #3").unwrap(),
            FormulaParser::new().parse("(#1 / #2) / #3").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 / #2 / #3").unwrap(),
            AST::div(AST::div(AST::Substep(1), AST::Substep(2)), AST::Substep(3))
        );
    }

    #[test]
    fn test_precedence_of_mult() {
        assert_eq!(
            FormulaParser::new().parse("#1 + #2 * #3").unwrap(),
            FormulaParser::new().parse("#1 + (#2 * #3)").unwrap()
        );
        assert_eq!(
            FormulaParser::new().parse("#1 + #2 * #3").unwrap(),
            AST::sum(AST::Substep(1), AST::mult(AST::Substep(2), AST::Substep(3)))
        );
    }
}
