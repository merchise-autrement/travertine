use crate::core::types::TravertineTypes;

/// The trait of any demand suitable to be computed by the travertine runtime.
///
/// For the time being we expect demands with a single request.  That's why we
/// don't have a `requests` in this trait.  To make this trait a little more
/// general we made the 'DateTime' type and associated type.
pub trait PriceDemand<C: TravertineTypes> {
    fn date(&self) -> C::DateTime;
    fn quantity(&self) -> f64 {
        1.0
    }
    fn start_date(&self) -> C::DateTime {
        self.date()
    }
    fn attr<K>(&self, k: K) -> Option<C::CustomValue>
    where
        K: Into<C::AttrId>;
}

pub trait ReplaceablePriceDemand<C: TravertineTypes>: PriceDemand<C> {
    fn replace_attr<K>(&self, attr: K, value: C::CustomValue) -> Self
    where
        K: Into<C::AttrId>;
}
