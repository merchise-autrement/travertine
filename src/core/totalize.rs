//! Wrapper for partial orders.
use std::cmp::Ordering;

#[inline]
pub(crate) fn totalized_cmp<T: PartialOrd>(l: &T, r: &T) -> Ordering {
    l.partial_cmp(r).unwrap_or(Ordering::Equal)
}

#[inline]
pub(crate) fn totalized_eq<T: PartialOrd>(l: &T, r: &T) -> bool {
    totalized_cmp(l, r) == Ordering::Equal
}

#[inline]
pub(crate) fn totalized_less_or_equal<T: PartialOrd>(l: &T, r: &T) -> bool {
    let order = totalized_cmp(l, r);
    order == Ordering::Equal || order == Ordering::Less
}

/// Allows to wrap non-totally ordered items into a total order
///
/// Every non-comparable value in the underlying type will be considered
/// Equal.
///
/// It's mainly here because we want to support Interval over floating point
/// number which don't implement [Ord] nor [Eq].  But the case when two
/// floating point numbers are not comparable is with NaN and infinities.
/// Interval should never be used in such a way that you end up with such
/// boundaries.
///
/// That being said, two *uncomparable* values are now magically Equal.
///
/// [Totalized] implements [From] for all [PartialOrd], but you better know what
/// your doing.
#[derive(Debug)]
pub struct Totalized<T: PartialOrd> {
    pub item: T,
}

impl<T: PartialOrd> PartialEq for Totalized<T> {
    fn eq(&self, other: &Self) -> bool {
        totalized_eq(&self.item, &other.item)
    }
}
impl<T: PartialOrd> Eq for Totalized<T> {}

impl<T: PartialOrd> PartialOrd for Totalized<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
impl<T: PartialOrd> Ord for Totalized<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        totalized_cmp(&self.item, &other.item)
    }
}

impl<T: PartialOrd> From<T> for Totalized<T> {
    fn from(item: T) -> Self {
        Totalized { item }
    }
}

impl<T: Clone + PartialOrd> Clone for Totalized<T> {
    fn clone(&self) -> Self {
        Totalized {
            item: self.item.clone(),
        }
    }
}
impl<T: Copy + PartialOrd> Copy for Totalized<T> {}

macro_rules! primitive_into_impl {
    ($t:ty) => {
        #[allow(clippy::from_over_into)]
        impl Into<$t> for Totalized<$t> {
            fn into(self) -> $t {
                self.item
            }
        }
    };
}

primitive_into_impl!(f32);
primitive_into_impl!(f64);

#[cfg(test)]
mod tests {
    use super::*;
    use proptest::prelude::*;

    proptest! {
        #[test]
        fn convert_f32_to_totals(a: f32, b: f32) {
            let ta: Totalized<f32> = a.into();
            let tb: Totalized<f32> = b.into();
            assert_eq!(a.partial_cmp(&b).unwrap_or(Ordering::Equal), ta.cmp(&tb))
        }

        #[test]
        fn convert_f64_to_totals(a: f64, b: f64) {
            let ta: Totalized<f64> = a.into();
            let tb: Totalized<f64> = b.into();
            assert_eq!(a.partial_cmp(&b).unwrap_or(Ordering::Equal), ta.cmp(&tb))
        }

        #[test]
        fn convert_totals_back_to_f64(a: f64) {
            let ta: Totalized<f64> = a.into();
            let b: f64 = ta.into();
            assert!((a - b).abs() < 1e-10f64);
        }

    }
}
