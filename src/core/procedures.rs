//! Procedures
//!
//! Procedures are the basic building block in the language.  You may regard
//! the procedure as the basic unit to form expressions.  Other units like
//! predicates are not expressions by their own.
//!
//! Procedures are kind of equivalent to keywords (or syntax) of a programming
//! language.  For instance the Branching procedure is like the sentence
//! `match` (however limited).

use either::Either;
use rust_decimal::prelude::*;
use std::collections::HashMap;

use crate::core::demand::PriceDemand;
pub(crate) use crate::core::formulae::ast::Formula as FormulaEntry;
use crate::core::predicates::Predicate;
use crate::core::types::TravertineTypes;
use crate::core::vm::{ProcedureIndex, VMResult, VM};

pub type Environment<C> = HashMap<<C as TravertineTypes>::VariableName, f64>;

/// A procedure definition.
///
/// Procedures are at the core of the language.  From a high-level view they
/// represent the operations possible in the language.  So, in a way, they are
/// kinda the AST of the language, but also the runable code.
///
/// Procedures are executed by passing a Demand and an Environment, their
/// execution returns either Undefined or a number (f64).
#[derive(Clone, Debug)]
pub enum Procedure<C: TravertineTypes> {
    /// Returns a constant value disregarding the demand and environment.
    ReturnConstant(f64),

    /// Returns Undefined disregarding the demand and environment.
    ReturnUndefined(Option<String>),

    /// Returns the value of an attribute in the demand.
    ///
    /// If the demand constains the same attribute many times return the first
    /// one.  If the values is not a number return Undefined.
    GetAttribute(C::AttrId),

    /// Returns the value of a variable from the environment.
    ///
    /// If the variable is not in the environment, return the default value.
    GetVariable(C::VariableName, f64),

    /// Executes the sub-procedure and rounds the result to a given precision.
    ///
    /// If the sub-procedure returns Undefined, return Undefined as well.
    Round(u8, RoundingMethod, ProcedureIndex),

    /// Executes the sub-procedure and return the minimum integral value that is
    /// greater or equal to the result.   If the result is Undefined, return
    /// Undefined.
    Ceil(ProcedureIndex),

    /// Executes the sub-procedure and return the maximum integral value that is
    /// lesser or equal to the result.   If the result is Undefined, return
    /// Undefined.
    Floor(ProcedureIndex),

    /// Returns the result of the sub-procedure extending the current
    /// environment with new values.
    SetEnv(Environment<C>, ProcedureIndex),

    /// Returns the result of the sub-procedure extending the current
    /// environment with default values.
    SetFallback(Environment<C>, ProcedureIndex),

    /// Executes the first sub-procedure for which the associated predicate
    /// returns true.  If no predicate matches, return Undefined.
    MatchBranch(Vec<(Predicate<C>, ProcedureIndex)>),

    /// Executes the first sub-procedures for which the associated predicate
    /// returns True.  If selected sub-procedure *signals* a backtracking (from
    /// a nested Backtracking), check the remaining branches in the same way. If
    /// no predicate matches, or all the sub-procedures backtrack, return
    /// Undefined if there's no Backtrack in the call stack, otherwise signal a
    /// backtracking.
    MatchBacktrackingBranch(Vec<(Predicate<C>, ProcedureIndex)>),

    /// Return the result of computing a simple formula.
    ///
    /// The language of the formulae is presented in module formulae.rs.  The
    /// sub-procedures are only executed on-demand and once regardless of how
    /// many times they appear in the formula.
    Formula(FormulaEntry<C>, Vec<ProcedureIndex>),

    /// Return the value of the sub-procedure.
    ///
    /// The Python layer have procedures like LoopProcedure, MapReduce, etc.
    /// But those procedures won't affect the way the price tables are
    /// generated.  All those procedures are translated to this.
    Identity(ProcedureIndex),

    /// Return the first row that matches all conditions
    #[allow(clippy::type_complexity)]
    Matrix(Vec<(Vec<MatrixCell<C>>, Either<FormulaEntry<C>, f64>)>),
}

#[allow(clippy::upper_case_acronyms)]
#[allow(non_camel_case_types)]
#[derive(Copy, Clone, Debug)]
pub enum RoundingMethod {
    UP,
    DOWN,
    HALF_UP,
}

impl RoundingMethod {
    pub fn round(&self, value: f64, precision: u8) -> f64 {
        Decimal::from_f64(value)
            .unwrap()
            .round_dp_with_strategy(precision as u32, self.strategy())
            .to_f64()
            .unwrap()
    }

    pub(crate) fn strategy(&self) -> RoundingStrategy {
        match self {
            Self::UP => RoundingStrategy::AwayFromZero,
            Self::DOWN => RoundingStrategy::ToZero,
            Self::HALF_UP => RoundingStrategy::MidpointAwayFromZero,
        }
    }
}

impl<C: TravertineTypes> Procedure<C> {
    pub fn vm_has_subprocedures(&self, vm: &VM<C>) -> VMResult {
        use Procedure::*;

        match self {
            Floor(procedure)
            | Ceil(procedure)
            | Round(_, _, procedure)
            | SetEnv(_, procedure)
            | SetFallback(_, procedure)
            | Identity(procedure) => vm.has_procedure(*procedure),
            MatchBranch(branches) | MatchBacktrackingBranch(branches) => {
                let procedures: Vec<_> = branches.iter().map(|(_, proc)| *proc).collect();
                vm.has_all_procedures(&procedures)
            }
            Formula(_, procedures) => vm.has_all_procedures(procedures),
            _ => Ok(()),
        }
    }
}

#[derive(Clone, Debug)]
pub enum MatrixCondition<C: TravertineTypes> {
    DemandDateInRange(C::DateTime, C::DateTime),
    DemandDateIs(C::DateTime),
    ExecutionDateInRange(C::DateTime, C::DateTime),
    ExecutionDateIs(C::DateTime),
    QuantityInRange(f64, f64),
    QuantityIs(f64),
    AttributeInRange(C::AttrId, C::CustomValue, C::CustomValue),
    AttributeIs(C::AttrId, C::CustomValue),
}

#[derive(Clone, Debug)]
pub enum MatrixCell<C: TravertineTypes> {
    Condition(MatrixCondition<C>),
    Variable(C::VariableName, f64),
    Default(C::VariableName, f64),
}

impl<C: TravertineTypes> MatrixCondition<C> {
    pub(crate) fn matches(&self, demand: &impl PriceDemand<C>) -> bool {
        match self {
            Self::DemandDateInRange(start, end) => {
                let date = &demand.date();
                start <= date && date < end
            }
            Self::DemandDateIs(date) => date == &demand.date(),
            Self::ExecutionDateInRange(start, end) => {
                let date = &demand.start_date();
                start <= date && date < end
            }
            Self::ExecutionDateIs(date) => date == &demand.start_date(),
            Self::QuantityInRange(min, max) => {
                let quantity = &demand.quantity();
                min <= quantity && quantity < max
            }
            // TODO: Try compare as close as Python does.
            Self::QuantityIs(quantity) => (quantity - demand.quantity()).abs() < 1e-9,
            Self::AttributeInRange(attr, lower, upper) => {
                if let Some(ref val) = demand.attr(attr.clone()) {
                    lower <= val && val < upper
                } else {
                    false
                }
            }
            Self::AttributeIs(attr, expected) => {
                if let Some(ref val) = demand.attr(attr.clone()) {
                    val == expected
                } else {
                    false
                }
            }
        }
    }
}
