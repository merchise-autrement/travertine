use crate::core::demand::PriceDemand;
use crate::core::types::TravertineTypes;

/// Predicates allow to put conditions on the demand to vary prices.
#[derive(Clone, Debug)]
pub enum Predicate<C: TravertineTypes> {
    /// Test that demand's date is within the given range.
    Validity(Option<C::DateTime>, Option<C::DateTime>),

    /// Test that execution date of the item is within the given range.
    Execution(Option<C::DateTime>, Option<C::DateTime>),

    /// Test that requested quantity is within the given range.
    Quantity(Option<f64>, Option<f64>),

    /// Test that the value of an attribute of the demand is within a given
    /// range.
    AttributeInRange(C::AttrId, Option<C::CustomValue>, Option<C::CustomValue>),

    /// Test that the value of an attribute of the demand matches a value.
    MatchesAttributes(C::AttrId, C::CustomValue),

    /// Always true
    Otherwise,
}

macro_rules! open_range_contains {
    ($upper: expr, $lower: expr, $member: expr) => {
        match ($upper, $lower) {
            (Some(u), Some(l)) => (u..l).contains(&$member),
            (Some(u), None) => (u..).contains(&$member),
            (None, Some(l)) => (..l).contains(&$member),
            (None, None) => true,
        }
    };
}

impl<C: TravertineTypes> Predicate<C> {
    #[inline]
    pub fn execute_predicate(&self, demand: &impl PriceDemand<C>) -> bool {
        use Predicate::*;

        match self {
            Otherwise => true,
            Validity(upper, lower) => open_range_contains!(upper, lower, &demand.date()),
            Execution(upper, lower) => open_range_contains!(upper, lower, &demand.start_date()),
            Quantity(upper, lower) => open_range_contains!(upper, lower, &demand.quantity()),
            AttributeInRange(name, upper, lower) => {
                if let Some(val) = demand.attr::<C::AttrId>(name.clone()) {
                    open_range_contains!(upper, lower, &val)
                } else {
                    false
                }
            }
            MatchesAttributes(name, expected) => {
                if let Some(value) = demand.attr::<C::AttrId>(name.clone()) {
                    value == *expected
                } else {
                    false
                }
            }
        }
    }
}
