//! The virtual machine
use crate::core::demand::PriceDemand;
use crate::core::formulae::ast::{BinaryOperator, AST};
use crate::core::predicates::Predicate;
use crate::core::prelude::{MatrixCell, MatrixCondition};
use crate::core::procedures::{Environment, FormulaEntry, Procedure};
use crate::core::result::ProcessResult;
use crate::core::types::TravertineTypes;

use either::{Left, Right};
use std::{collections::HashMap, convert::TryInto};

/// The virtual machine is the top-level structure that holds all procedures.
///
/// To *run* a given procedure you need to setup a process with a given
/// demand.  Once the process ends, the result can be retrieved from the
/// process.  The process may linger for a while, but it cannot be ran any
/// more (it would be pointless since it will yield the same result.)
///
/// The method [VM::execute] is a shortcut.
///
#[derive(Clone)]
pub struct VM<C: TravertineTypes> {
    procedures: HashMap<ProcedureIndex, Procedure<C>>,
}
pub type ProcedureIndex = usize;

#[derive(Clone, Copy, Debug)]
pub enum VMError {
    DuplicatedProcedure(ProcedureIndex),
    MissingProcedure(ProcedureIndex),
}

impl std::fmt::Display for VMError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
impl std::error::Error for VMError {}
pub type VMResult = Result<(), VMError>;

impl<C: TravertineTypes> VM<C> {
    /// Create an empty VM.
    pub fn new() -> Self {
        Self {
            procedures: HashMap::new(),
        }
    }

    /// Create an empty VM but with some capacity already allocated.
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            procedures: HashMap::with_capacity(capacity),
        }
    }

    pub(crate) fn has_procedure(&self, procedure: ProcedureIndex) -> VMResult {
        self.procedures
            .get(&procedure)
            .map_or_else(|| Err(VMError::MissingProcedure(procedure)), |_| Ok(()))
    }

    pub(crate) fn has_all_procedures(&self, procedures: &[ProcedureIndex]) -> VMResult {
        for procedure in procedures {
            self.has_procedure(*procedure)?
        }
        Ok(())
    }

    /// Adds a procedure to the virtual machine.
    ///
    /// Each procedure is identified by a ProcedureIndex (`usize`).  It's an
    /// error to try to reuse the same index.
    ///
    /// If the procedure needs sub-procedures they must be added before adding
    /// this one.
    #[allow(clippy::map_entry)]
    pub fn add_procedure(
        &mut self,
        index: ProcedureIndex,
        proc: Procedure<C>,
    ) -> Result<ProcedureIndex, VMError> {
        proc.vm_has_subprocedures(self)?;

        if self.procedures.contains_key(&index) {
            Err(VMError::DuplicatedProcedure(index))
        } else {
            self.procedures.insert(index, proc);
            Ok(index)
        }
    }

    /// Create and execute a [Process] for the given demand.
    ///
    /// # Panics
    ///
    /// If the process ends without producing a value.  This is not actually
    /// expected, though.  It will more likely be the result of a bug in this
    /// crate.
    pub fn execute(&self, demand: &impl PriceDemand<C>, proc: ProcedureIndex) -> ProcessResult {
        let mut process = Process::new(self, proc);
        process.execute(demand);
        process.result.unwrap()
    }
}

impl<C: TravertineTypes> Default for VM<C> {
    fn default() -> Self {
        Self::new()
    }
}

pub struct Process<'vm, C: TravertineTypes> {
    vm: &'vm VM<C>,
    procedure: ProcedureIndex,

    state: ProcessState,
    result: Option<ProcessResult>,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum ProcessState {
    /// The process has been just created and done nothing yet.  The process
    /// stack is empty, but initialized, the demand is set.  Everything is
    /// good to go, we're just waiting the order.
    New,

    /// The process is running.
    Running,

    /// The process finished normally.  The stack is possibly empty, and the
    /// result should be Some(value).
    Finished,
}

#[derive(Clone, Copy, Debug)]
enum ProcessError<T> {
    BacktrackingSignal,
    #[allow(dead_code)]
    Error(T),
}

impl<'vm, C: TravertineTypes> Process<'vm, C> {
    fn new(vm: &'vm VM<C>, procedure: ProcedureIndex) -> Self {
        Self {
            vm,
            procedure,
            state: ProcessState::New,
            result: None,
        }
    }

    fn execute(&mut self, demand: &impl PriceDemand<C>) {
        self.state = ProcessState::Running;
        let env: HashMap<C::VariableName, f64> = HashMap::new();
        let frame = ProcessFrame::new(env);
        let result = match self.execute_in_frame(&frame, demand, self.procedure) {
            Ok(res) => res,
            Err(procerr) => match procerr {
                ProcessError::Error(_) => {
                    ProcessResult::Undefined(Some("An error occurred".to_string()))
                }
                ProcessError::BacktrackingSignal => {
                    panic!("We should get a backtracking signal here!")
                }
            },
        };
        self.result = Some(result);
        self.state = ProcessState::Finished;
    }

    fn execute_in_frame(
        &mut self,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
        procedure: ProcedureIndex,
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        let procedure = self.vm.procedures.get(&procedure).unwrap();
        match procedure {
            Procedure::ReturnUndefined(s) => Ok(ProcessResult::Undefined(s.to_owned())),
            Procedure::ReturnConstant(c) => Ok(ProcessResult::Value(*c)),
            Procedure::SetEnv(env, procedure) => {
                let frame = frame.spawn_with_variables(env);
                self.execute_in_frame(&frame, demand, *procedure)
            }
            Procedure::SetFallback(env, procedure) => {
                let frame = frame.spawn_with_defaults(env);
                self.execute_in_frame(&frame, demand, *procedure)
            }
            Procedure::Ceil(proc) => {
                let res = self.execute_in_frame(frame, demand, *proc)?;
                match res {
                    ProcessResult::Value(c) => Ok(ProcessResult::Value(c.ceil())),
                    _ => Ok(res),
                }
            }
            Procedure::Floor(proc) => {
                let res = self.execute_in_frame(frame, demand, *proc)?;
                match res {
                    ProcessResult::Value(c) => Ok(ProcessResult::Value(c.floor())),
                    _ => Ok(res),
                }
            }
            Procedure::Round(prec, method, proc) => {
                let res = self.execute_in_frame(frame, demand, *proc)?;
                match res {
                    ProcessResult::Value(c) => Ok(ProcessResult::Value(method.round(c, *prec))),
                    _ => Ok(res),
                }
            }
            Procedure::Identity(proc) => self.execute_in_frame(frame, demand, *proc),
            Procedure::Formula(formula, procedures) => {
                formula.execute_in_frame(self, frame, demand, procedures)
            }
            Procedure::MatchBranch(branches) => {
                let procedure = BranchingProcedure::new(branches);
                procedure.execute_in_frame(self, frame, demand)
            }
            Procedure::MatchBacktrackingBranch(branches) => {
                // The first backtracking frame will have level 1, this frame
                // will return Ok(Undefined) if no matching branch is found.
                // Frames with higher backtracking levels will signal the
                // backtracking by return Err<ProcessError::BacktrackingSignal>.
                let frame = frame.spawn_backtracking_frame();
                let procedure = BranchingProcedure::new(branches);
                procedure.execute_in_frame_with_backtracking(self, &frame, demand)
            }
            Procedure::GetVariable(name, default) => {
                let result = frame.environment.get(name).unwrap_or(default);
                Ok(ProcessResult::Value(*result))
            }
            Procedure::GetAttribute(name) => {
                Ok(if let Some(res) = demand.attr::<C::AttrId>(name.clone()) {
                    if let Ok(num) = res.try_into() {
                        ProcessResult::Value(num)
                    } else {
                        ProcessResult::Undefined(Some(format!(
                            "Attribute {} had the wrong type",
                            name
                        )))
                    }
                } else {
                    ProcessResult::Undefined(Some(format!("Missing attribute {}", name)))
                })
            }
            Procedure::Matrix(rows) => {
                for (headers, res) in rows {
                    let mut conditions =
                        headers
                            .iter()
                            .filter_map::<MatrixCondition<_>, _>(|h| match h {
                                MatrixCell::Condition(x) => Some(x.clone()),
                                _ => None,
                            });
                    let variables: Environment<C> = headers
                        .iter()
                        .filter_map(|h| match h {
                            MatrixCell::Variable(var, value) => Some((var.clone(), *value)),
                            _ => None,
                        })
                        .collect();
                    let defaults: Environment<C> = headers
                        .iter()
                        .filter_map(|h| match h {
                            MatrixCell::Default(var, value) => Some((var.clone(), *value)),
                            _ => None,
                        })
                        .collect();
                    if conditions.all(|cond| cond.matches(demand)) {
                        return match res {
                            Left(formula) => {
                                let procedures: [ProcedureIndex; 0] = [];
                                formula.execute_with_envs(
                                    &variables,
                                    &defaults,
                                    self,
                                    frame,
                                    demand,
                                    &procedures,
                                )
                            }
                            Right(val) => Ok(ProcessResult::Value(*val)),
                        };
                    }
                }
                if frame.backtracking_level > 0 {
                    Err(ProcessError::BacktrackingSignal)
                } else {
                    Ok(ProcessResult::annotated_undefined(
                        "Not matched row in matrix",
                    ))
                }
            }
        }
    }
}

// A frame must be created whenever the environment, or the demand changes.
// Backtracking procedures also create a frame.  Other kind of procedures
// run in the same frame.
struct ProcessFrame<C: TravertineTypes> {
    environment: Environment<C>,
    backtracking_level: u8,
}

impl<C: TravertineTypes> ProcessFrame<C> {
    fn new(environment: Environment<C>) -> Self {
        Self {
            environment,
            backtracking_level: 0,
        }
    }

    fn spawn_with_variables(&self, env: &Environment<C>) -> Self {
        let mut newenv = self.environment.clone();
        let env = env.clone();
        newenv.extend(env);
        Self {
            environment: newenv,
            backtracking_level: self.backtracking_level,
        }
    }

    fn spawn_with_defaults(&self, env: &Environment<C>) -> Self {
        let mut env = env.clone();
        env.extend(self.environment.clone());
        Self {
            environment: env,
            backtracking_level: self.backtracking_level,
        }
    }

    fn spawn_backtracking_frame(&self) -> Self {
        Self {
            environment: self.environment.clone(),

            // Up to 255 levels of backtracking before panicking!
            backtracking_level: self.backtracking_level.checked_add(1).unwrap(),
        }
    }
}

impl<C: TravertineTypes> FormulaEntry<C> {
    fn execute_in_frame(
        &self,
        process: &mut Process<C>,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
        procedures: &[ProcedureIndex],
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        FormulaEntry::eval_ast(&self.ast, process, frame, demand, procedures)
    }

    fn execute_with_envs(
        &self,
        variables: &Environment<C>,
        defaults: &Environment<C>,
        process: &mut Process<C>,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
        procedures: &[ProcedureIndex],
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        let frame = frame
            .spawn_with_variables(variables)
            .spawn_with_defaults(defaults);
        self.execute_in_frame(process, &frame, demand, procedures)
    }

    fn eval_ast(
        node: &AST,
        process: &mut Process<C>,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
        procedures: &[ProcedureIndex],
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        match node {
            AST::Variable(name) => frame
                .environment
                .get(&C::resolve_variable_name(name))
                .map_or(
                    Ok(ProcessResult::annotated_undefined(format!(
                        "Varible {} is missing",
                        name
                    ))),
                    |n| Ok(ProcessResult::value(*n)),
                ),

            AST::LiteralNumber(n) => Ok(ProcessResult::value(*n)),
            AST::Substep(index) => {
                let ix = *index - 1;
                if ix < procedures.len() {
                    let proc = procedures[ix];
                    process.execute_in_frame(frame, demand, proc)
                } else {
                    Ok(ProcessResult::annotated_undefined(format!(
                        "IndexError {}",
                        index
                    )))
                }
            }

            AST::BinaryOperation(op, left, right) => {
                let lhs = FormulaEntry::eval_ast(left, process, frame, demand, procedures)?;
                let rhs = FormulaEntry::eval_ast(right, process, frame, demand, procedures)?;
                match op {
                    BinaryOperator::Add => Ok(lhs + rhs),
                    BinaryOperator::Sub => Ok(lhs - rhs),
                    BinaryOperator::Mult => Ok(lhs * rhs),
                    BinaryOperator::Div => Ok(lhs / rhs),
                }
            }

            AST::Negation(node) => {
                let res = FormulaEntry::eval_ast(node, process, frame, demand, procedures)?;
                Ok(-res)
            }
        }
    }
}

struct BranchingProcedure<C: TravertineTypes> {
    branches: Vec<(Predicate<C>, ProcedureIndex)>,
}

impl<C: TravertineTypes> BranchingProcedure<C> {
    fn new(branches: &[(Predicate<C>, ProcedureIndex)]) -> Self {
        let branches: Vec<_> = branches.to_vec();
        Self { branches }
    }

    fn execute_in_frame(
        &self,
        process: &mut Process<C>,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        for (predicate, procedure) in &self.branches {
            if predicate.execute_predicate(demand) {
                return process.execute_in_frame(frame, demand, *procedure);
            }
        }
        Ok(ProcessResult::annotated_undefined("No branch matched"))
    }

    fn execute_in_frame_with_backtracking(
        &self,
        process: &mut Process<C>,
        frame: &ProcessFrame<C>,
        demand: &impl PriceDemand<C>,
    ) -> Result<ProcessResult, ProcessError<VMError>> {
        for (predicate, procedure) in &self.branches {
            if predicate.execute_predicate(demand) {
                match process.execute_in_frame(frame, demand, *procedure) {
                    Ok(res) => return Ok(res),
                    Err(procerr) => match procerr {
                        ProcessError::BacktrackingSignal => continue,
                        ProcessError::Error(err) => return Err(ProcessError::Error(err)),
                    },
                };
            }
        }
        if frame.backtracking_level > 1 {
            Err(ProcessError::BacktrackingSignal)
        } else {
            Ok(ProcessResult::annotated_undefined("No matching branch"))
        }
    }
}
