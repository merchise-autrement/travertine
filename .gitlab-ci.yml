variables:
  RUST_IMAGE: rust:1.80

stages:
  - test
  - build
  - coverage
  - publish
  - clean up
  - deploy


workflow:
  auto_cancel:
    on_new_commit: interruptible
    on_job_failure: all

.x-code: &code
  - pyproject.toml
  - requirements-dev-*.lock
  - python/**/*.py
  - python/**/*.pyi
  - src/**/*.rs
  - Cargo.toml
  - .gitlab-ci.yml

.x-rust-code: &rust-code
  - pyproject.toml
  - src/**/*.rs
  - Cargo.toml
  - .gitlab-ci.yml

.x-python-code: &python-code
  - requirements-dev-*.lock
  - pyproject.toml
  - python/**/*.py
  - python/**/*.pyi
  - Cargo.toml
  - .gitlab-ci.yml

.x-code-and-docs: &code-and-docs
  - pyproject.toml
  - python/**/*.py
  - python/**/*.pyi
  - src/**/*.rs
  - Cargo.toml
  - .gitlab-ci.yml
  - docs/**/*.rst

check rust format:
  interruptible: true
  image: $RUST_IMAGE
  tags:
    - docker
  stage: test
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      rm -r rust-toolchain
      rustup component add rustfmt
      cargo fmt --all -- --check
  cache:
    paths:
      - .cargo
      - target
    key: $CI_JOB_NAME
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *rust-code

check python format:
  interruptible: true
  image: python:3.8
  tags:
    - docker
  stage: test
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source "$CARGO_HOME/env"
      pip install -r requirements-dev-py38.lock
      ruff format --check python/ && isort -c --df python/
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *python-code

lint rust:
  interruptible: true
  image: $RUST_IMAGE
  allow_failure: true
  tags:
    - docker
  stage: test
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      rustup component add clippy
      cargo clippy --all-features --all --tests -- -D clippy::all
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *rust-code
  cache:
    paths:
      - .cargo
      - target
    key: $CI_JOB_NAME


lint python:
  interruptible: true
  image: python:3.12
  tags:
    - docker
  stage: test
  script:
    - |
      set -x
      pip install ruff==0.2.1 isort~=5.13.2
      ruff check python/
      ruff format --check python/
      isort --check python/
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *python-code

static check python:
  interruptible: true
  image: python:3.8
  stage: test
  tags:
    - docker
  script:
    - |
      set -x
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source "$CARGO_HOME/env"
      pip install -r requirements-dev-py38.lock
      mypy -p travertine --config mypy.ini
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *python-code
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *python-code

audit dependencies changes:
  interruptible: true
  image: $RUST_IMAGE
  tags:
    - docker
  stage: test
  script:
    - |
      cargo install cargo-audit
      cargo audit
  tags:
    - docker
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: &cargo-files
        - Cargo.toml
        - Cargo.lock
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *cargo-files

.build_python_extension: &build_python_extension
  image: quay.io/pypa/manylinux2014_x86_64
  tags:
    - docker
  stage: build
  resource_group: "$CI_PROJECT_NAME - $CI_JOB_NAME"
  script:
    - |
      set -x
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source "$CARGO_HOME/env"
      for pybin in /opt/python/cp3{8,9,10,11,12}*/bin; do
          "${pybin}/pip" install 'maturin>=1.4,<1.5'
          "${pybin}/maturin" build --out dist -i "${pybin}/python" --target x86_64-unknown-linux-gnu
      done
      for wheel in dist/*.whl; do
          auditwheel repair "${wheel}"
      done
  artifacts:
    paths:
      - dist/*.whl
      - python/tests
      - docs/
    expire_in: 120 hours

build wheels:
  <<: *build_python_extension
  interruptible: true
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_COMMIT_TAG =~ /^\d+(\.\d+)*(|a\d+|b\d+|rc\d+)?(\.post\d+|\.dev\d+)?$/
    - if: $CI_MERGE_REQUEST_IID
      changes: *code-and-docs
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *code-and-docs
    - when: never

.run_python_tests: &run_python_tests
  image: python:$PYTHON_VERSION
  tags:
    - docker
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source "$CARGO_HOME/env"
      pip install tox
      tox -e system-unit -- -n auto --maxprocesses=4
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *python-code
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *python-code

run all python tests:
  <<: *run_python_tests
  stage: test
  interruptible: true
  variables:
    PYTEST_ARGS: "-m 'not regression'"
  parallel:
    matrix:
      - PYTHON_VERSION: ["3.8-buster", "3.9-buster", "3.10-buster", "3.11-buster", "3.12-bullseye"]

run all python tests in Python (with regression):
  <<: *run_python_tests
  stage: test
  interruptible: true
  allow_failure: true
  variables:
    PYTEST_ARGS: "-m regression"
  parallel:
    matrix:
      - PYTHON_VERSION: ["3.8-buster"]

.run_coverage: &run_coverage
  image: python:$PYTHON_VERSION
  tags:
    - docker
  cache:
    paths:
      - .cargo
      - target
    key: $CI_JOB_NAME
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source $CARGO_HOME/env
      pip install tox
      tox -e system-coverage -- -n auto

coverage:
  <<: *run_coverage
  stage: coverage
  coverage: '/TOTAL.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  interruptible: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    PYTHON_VERSION: "3.8"

.build_documentation: &build_documentation
  image: python:3.8
  tags:
    - docker
  script:
    - |
      set -x
      apt-get update
      apt-get install -y --no-install-recommends git

      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source $CARGO_HOME/env

      pip install -r requirements-dev-py38.lock
      make -C docs html
      mkdir -p public
      cp -rf docs/build/html/* public/
  cache:
    paths:
      - .cargo
      - target
    key: $CI_JOB_NAME


.run_doctests: &run_doctests
  image: python:$PYTHON_VERSION
  tags:
    - docker
  script:
    - |
      export CARGO_HOME=$PWD/.cargo
      curl https://sh.rustup.rs -sSf | sh -s -- -y --default-toolchain=stable
      source "$CARGO_HOME/env"
      pip install tox
      tox -e system-doctest

run doctests in Python 3.8:
  <<: *run_doctests
  stage: test
  interruptible: true
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *code-and-docs
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes: *code-and-docs
  variables:
    PYTHON_VERSION: "3.8"

build documentation:
  <<: *build_documentation
  interruptible: true
  stage: build
  needs: []
  artifacts:
    paths:
      - docs/build/html
    expire_in: 4 hours
  rules: &gitlab-merchise-doc-rules
    - if: $CI_COMMIT_TAG || $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_MERGE_REQUEST_IID
      changes: *code-and-docs

pages:
  <<: *build_documentation
  stage: deploy
  needs: []
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_SERVER_HOST != "gitlab.com"
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH


.publish_pypi: &publish_pypi
  image: python:3.8
  tags:
    - docker
  stage: publish
  script:
    - pip install twine
    - twine upload --verbose dist/*.whl
    - rm dist/*.whl


publish Python extensions in PyPI:
  <<: *publish_pypi
  variables:
    GIT_STRATEGY: none
    TWINE_USERNAME: $PYPI_USERNAME
    TWINE_PASSWORD: $PYPI_PASSWORD
  needs:
    - build wheels
  environment:
    name: pypi
    url: https://pypi.org/project/$CI_PROJECT_NAME
  rules:
    - if: $CI_SERVER_HOST != "gitlab.merchise.org"
      when: never
    - if: $CI_COMMIT_TAG =~ /^\d+(\.\d+)*(|a\d+|b\d+|rc\d+)?(\.post\d+|\.dev\d+)?$/


# Local Variables:
# tab-width: 2
# End:
